# McPat Integration for Pirafix and REPAIR #

This is a reuse of the code from H2020-COSSIM project. The licenses and copyrights follow from there. 
The original author is Andreas Brokalakis. 


This repository includes codes for the area and power calculations for my thesis. 

### What is this repository for? ###

*  Running tests from REPAIR and pirafix output to  
*  Scripts to test viability.
*  

### Running Gem5-Pirafix-McPat


### Who do I talk to? ###

* To discuss methods of integrating Pirafix into your control flow, send an email to:
first.last@cl.cam.ac.uk 
Jyothish Soman
* For McPat related concerns: Original author 
* For a philosophical discussions on the correctness of simulations: Your nearest comp-arch friends.
* For all other concerns, Peter Parker. 

