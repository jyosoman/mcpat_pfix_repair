/*****************************************************************************
 *                                McPAT
 *                      SOFTWARE LICENSE AGREEMENT
 *            Copyright 2012 Hewlett-Packard Development Company, L.P.
 *                          All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.”
 *
 ***************************************************************************/


#include <cstdio>
#include "xmlParser.h"
#include <string>
#include "XML_Parse.h"
#include <iostream>

using namespace std;

double getNodeDoubleData(XMLNode &xNode2, unsigned int i) {
    return std::strtof(xNode2.getChildNode("param", i).getAttribute("value"), nullptr);
}

double getNodeDoubleData(XMLNode &xNode2, unsigned int i, const char *str) {
    return std::strtof(xNode2.getChildNode(str, i).getAttribute("value"), nullptr);
}


int getNodeData(XMLNode &xNode4, unsigned int k) {
    return (int) std::strtol(xNode4.getChildNode("param", k).getAttribute("value"), nullptr, 0);
}

bool compareString(XMLNode &xNode, unsigned int index, const char *data) {
    return strcmp(xNode.getChildNode("param", index).getAttribute("name"), data) == 0;
}

bool compareString(XMLNode &xNode, unsigned int index, const char *str, const char *data) {
    return strcmp(xNode.getChildNode(str, index).getAttribute("name"), data) == 0;
}

void ParseXML::parse(char *filepath) {
    unsigned int m, n;
    int NumofCom_4;
    int itmp;
    //Initialize all structures
    ParseXML::initialize();

    // this open and parse the XML file:
    XMLNode xMainNode = XMLNode::openFileHelper(filepath, "component"); //the 'component' in the first layer

    XMLNode xNode2 = xMainNode.getChildNode("component"); // the 'component' in the second layer
    //get all params in the second layer
    itmp = xNode2.nChildNode("param");
    for (unsigned int i = 0; i < itmp; i++) {
        if (compareString(xNode2, i, "number_of_cores")) {
            sys.number_of_cores = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "number_of_L1Directories")) {
            sys.number_of_L1Directories = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "number_of_L2Directories")) {
            sys.number_of_L2Directories = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "number_of_L2s")) {
            sys.number_of_L2s = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "Private_L2")) {
            sys.Private_L2 = (bool) getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "number_of_L3s")) {
            sys.number_of_L3s = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "number_of_NoCs")) {
            sys.number_of_NoCs = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "number_of_dir_levels")) {
            sys.number_of_dir_levels = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "domain_size")) {
            sys.domain_size = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "first_level_dir")) {
            sys.first_level_dir = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "homogeneous_cores")) {
            sys.homogeneous_cores = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "core_tech_node")) {
            sys.core_tech_node = getNodeDoubleData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "target_core_clockrate")) {
            sys.target_core_clockrate = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "target_chip_area")) {
            sys.target_chip_area = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "temperature")) {
            sys.temperature = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "number_cache_levels")) {
            sys.number_cache_levels = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "L1_property")) {
            sys.L1_property = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "L2_property")) {
            sys.L2_property = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "homogeneous_L2s")) {
            sys.homogeneous_L2s = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "homogeneous_L1Directories")) {
            sys.homogeneous_L1Directories = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "homogeneous_L2Directories")) {
            sys.homogeneous_L2Directories = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "L3_property")) {
            sys.L3_property = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "homogeneous_L3s")) {
            sys.homogeneous_L3s = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "homogeneous_ccs")) {
            sys.homogeneous_ccs = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "homogeneous_NoCs")) {
            sys.homogeneous_NoCs = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "Max_area_deviation")) {
            sys.Max_area_deviation = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "Max_power_deviation")) {
            sys.Max_power_deviation = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "device_type")) {
            sys.device_type = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "longer_channel_device")) {
            sys.longer_channel_device = (bool) getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "power_gating")) {
            sys.power_gating = (bool) getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "opt_dynamic_power")) {
            sys.opt_dynamic_power = (bool) getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "opt_lakage_power")) {
            sys.opt_lakage_power = (bool) getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "opt_clockrate")) {
            sys.opt_clockrate = (bool) getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "opt_area")) {
            sys.opt_area = (bool) getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "Embedded")) {
            sys.Embedded = (bool) getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "interconnect_projection_type")) {
            sys.interconnect_projection_type = getNodeData(xNode2, i) ? 0 : 1;
            continue;
        }
        if (compareString(xNode2, i, "machine_bits")) {
            sys.machine_bits = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "virtual_address_width")) {
            sys.virtual_address_width = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "physical_address_width")) {
            sys.physical_address_width = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "virtual_memory_page_size")) {
            sys.virtual_memory_page_size = getNodeData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "vdd")) {
            sys.vdd = getNodeDoubleData(xNode2, i);
            continue;
        }
        if (compareString(xNode2, i, "power_gating_vcc")) {
            sys.power_gating_vcc = getNodeDoubleData(xNode2, i);
            continue;
        }


    }

//	if (sys.Private_L2 && sys.number_of_cores!=sys.number_of_L2s)
//	{
//		cout<<"Private L2: Number of L2s must equal to Number of Cores"<<endl;
//		exit(0);
//	}

    itmp = xNode2.nChildNode("stat");
    for (unsigned int i = 0; i < itmp; i++) {
        if (compareString(xNode2, i, "stat" "total_cycles")) {
            sys.total_cycles = std::strtof(xNode2.getChildNode("stat", i).getAttribute("value"), nullptr);
            continue;
        }

    }

    //get the number of components within the second layer
    int NumofCom_3 = xNode2.nChildNode("component");
    XMLNode xNode3, xNode4; //define the third-layer(system.core0) and fourth-layer(system.core0.predictor) xnodes

    string strtmp;
    char chtmp[60];
    char chtmp1[60];
    chtmp1[0] = '\0';
    int OrderofComponents_3layer = 0;
    if (NumofCom_3 > OrderofComponents_3layer) {
        //___________________________get all system.core0-n________________________________________________
        if (sys.homogeneous_cores == 1) OrderofComponents_3layer = 0;
        else OrderofComponents_3layer = sys.number_of_cores - 1;
        for (unsigned int i = 0; i <= OrderofComponents_3layer; i++) {
            xNode3 = xNode2.getChildNode("component", i);
            if (xNode3.isEmpty() == 1) {
                printf("The value of homogeneous_cores or number_of_cores is not correct!");
                exit(0);
            } else {
                if (strstr(xNode3.getAttribute("name"), "core") != nullptr) {
                    { //For cpu0-cpui
                        //Get all params with system.core?
                        itmp = xNode3.nChildNode("param");
                        for (unsigned int k = 0; k < itmp; k++) {
                            if (compareString(xNode3, k, "clock_rate")) {
                                sys.core[i].clock_rate = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "opt_local")) {
                                sys.core[i].opt_local = (bool) getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "x86")) {
                                sys.core[i].x86 = (bool) getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "machine_bits")) {
                                sys.core[i].machine_bits = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "virtual_address_width") ==
                                0) {
                                sys.core[i].virtual_address_width = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k,
                                              "physical_address_width")) {
                                sys.core[i].physical_address_width = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "instruction_length") ==
                                0) {
                                sys.core[i].instruction_length = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "opcode_width")) {
                                sys.core[i].opcode_width = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "micro_opcode_width") ==
                                0) {
                                sys.core[i].micro_opcode_width = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "machine_type")) {
                                sys.core[i].machine_type = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k,
                                              "internal_datapath_width")) {
                                sys.core[i].internal_datapath_width = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k,
                                              "number_hardware_threads")) {
                                sys.core[i].number_hardware_threads = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "fetch_width")) {
                                sys.core[i].fetch_width = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k,
                                              "number_instruction_fetch_ports")) {
                                sys.core[i].number_instruction_fetch_ports = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "decode_width")) {
                                sys.core[i].decode_width = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "issue_width")) {
                                sys.core[i].issue_width = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "peak_issue_width")) {
                                sys.core[i].peak_issue_width = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "commit_width")) {
                                sys.core[i].commit_width = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "fp_issue_width")) {
                                sys.core[i].fp_issue_width = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "prediction_width")) {
                                sys.core[i].prediction_width = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "vdd")) {
                                sys.core[i].vdd = getNodeDoubleData(xNode2, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "power_gating_vcc")) {
                                sys.core[i].power_gating_vcc = getNodeDoubleData(xNode2, k);
                                continue;
                            }

                            if (compareString(xNode3, k, "pipelines_per_core") ==
                                0) {
                                strtmp.assign(xNode3.getChildNode("param", k).getAttribute("value"));
                                m = 0;
                                for (n = 0; n < strtmp.length(); n++) {
                                    if (strtmp[n] != ',') {
                                        sprintf(chtmp, "%c", strtmp[n]);
                                        strcat(chtmp1, chtmp);
                                    } else {
                                        sys.core[i].pipelines_per_core[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                        m++;
                                        chtmp1[0] = '\0';
                                    }
                                }
                                sys.core[i].pipelines_per_core[m] = (int) std::strtol(chtmp1, nullptr, 0);
//								m++;
                                chtmp1[0] = '\0';
                                continue;
                            }
                            if (compareString(xNode3, k, "pipeline_depth")) {
                                strtmp.assign(xNode3.getChildNode("param", k).getAttribute("value"));
                                m = 0;
                                for (n = 0; n < strtmp.length(); n++) {
                                    if (strtmp[n] != ',') {
                                        sprintf(chtmp, "%c", strtmp[n]);
                                        strcat(chtmp1, chtmp);
                                    } else {
                                        sys.core[i].pipeline_depth[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                        m++;
                                        chtmp1[0] = '\0';
                                    }
                                }
                                sys.core[i].pipeline_depth[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                //m++;
                                chtmp1[0] = '\0';
                                continue;
                            }

                            if (compareString(xNode3, k, "FPU")) {
                                strcpy(sys.core[i].FPU, xNode3.getChildNode("param", k).getAttribute("value"));
                                continue;
                            }
                            if (compareString(xNode3, k, "divider_multiplier") ==
                                0) {
                                strcpy(sys.core[i].divider_multiplier,
                                       xNode3.getChildNode("param", k).getAttribute("value"));
                                continue;
                            }
                            if (compareString(xNode3, k, "ALU_per_core")) {
                                sys.core[i].ALU_per_core = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "FPU_per_core")) {
                                sys.core[i].FPU_per_core = getNodeDoubleData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "MUL_per_core")) {
                                sys.core[i].MUL_per_core = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k,
                                              "instruction_buffer_size")) {
                                sys.core[i].instruction_buffer_size = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k,
                                              "decoded_stream_buffer_size")) {
                                sys.core[i].decoded_stream_buffer_size = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k,
                                              "instruction_window_scheme")) {
                                sys.core[i].instruction_window_scheme = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k,
                                              "instruction_window_size")) {
                                sys.core[i].instruction_window_size = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k,
                                              "fp_instruction_window_size")) {
                                sys.core[i].fp_instruction_window_size = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "ROB_size")) {
                                sys.core[i].ROB_size = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "archi_Regs_IRF_size") ==
                                0) {
                                sys.core[i].archi_Regs_IRF_size = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "archi_Regs_FRF_size") ==
                                0) {
                                sys.core[i].archi_Regs_FRF_size = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "phy_Regs_IRF_size") ==
                                0) {
                                sys.core[i].phy_Regs_IRF_size = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "phy_Regs_FRF_size") ==
                                0) {
                                sys.core[i].phy_Regs_FRF_size = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "rename_scheme")) {
                                sys.core[i].rename_scheme = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "checkpoint_depth")) {
                                sys.core[i].checkpoint_depth = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "register_windows_size") ==
                                0) {
                                sys.core[i].register_windows_size = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "LSU_order")) {
                                strcpy(sys.core[i].LSU_order, xNode3.getChildNode("param", k).getAttribute("value"));
                                continue;
                            }
                            if (compareString(xNode3, k, "store_buffer_size") ==
                                0) {
                                sys.core[i].store_buffer_size = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "load_buffer_size")) {
                                sys.core[i].load_buffer_size = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "memory_ports")) {
                                sys.core[i].memory_ports = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "Dcache_dual_pump")) {
                                strcpy(sys.core[i].Dcache_dual_pump,
                                       xNode3.getChildNode("param", k).getAttribute("value"));
                                continue;
                            }
                            if (compareString(xNode3, k, "RAS_size")) {
                                sys.core[i].RAS_size = getNodeData(xNode3, k);
                                continue;
                            }
                        }
                        //Get all stats with system.core?
                        itmp = xNode3.nChildNode("stat");
                        for (unsigned int k = 0; k < itmp; k++) {
                            if (compareString(xNode3, k, "stat" "total_instructions") ==
                                0) {
                                sys.core[i].total_instructions = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "int_instructions")) {
                                sys.core[i].int_instructions = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat"
                                                         "reexecuted_int_instructions")) {
                                sys.core[i].reexecuted_int_instructions = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "fp_instructions")) {
                                sys.core[i].fp_instructions = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat"
                                                         "reexecuted_fp_instructions")) {
                                sys.core[i].reexecuted_fp_instructions = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "branch_instructions") ==
                                0) {
                                sys.core[i].branch_instructions = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "branch_mispredictions") ==
                                0) {
                                sys.core[i].branch_mispredictions = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "committed_instructions") ==
                                0) {
                                sys.core[i].committed_instructions = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat"
                                                         "committed_int_instructions")) {
                                sys.core[i].committed_int_instructions = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat"
                                                         "committed_fp_instructions")) {
                                sys.core[i].committed_fp_instructions = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "load_instructions")) {
                                sys.core[i].load_instructions = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "store_instructions") ==
                                0) {
                                sys.core[i].store_instructions = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "total_cycles")) {
                                sys.core[i].total_cycles = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "idle_cycles")) {
                                sys.core[i].idle_cycles = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "busy_cycles")) {
                                sys.core[i].busy_cycles = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat"
                                                         "instruction_buffer_reads")) {
                                sys.core[i].instruction_buffer_reads = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat"
                                                         "instruction_buffer_write")) {
                                sys.core[i].instruction_buffer_write = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "ROB_reads")) {
                                sys.core[i].ROB_reads = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "ROB_writes")) {
                                sys.core[i].ROB_writes = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "rename_reads")) {
                                sys.core[i].rename_reads = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "rename_writes")) {
                                sys.core[i].rename_writes = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "fp_rename_reads")) {
                                sys.core[i].fp_rename_reads = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "fp_rename_writes")) {
                                sys.core[i].fp_rename_writes = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "inst_window_reads")) {
                                sys.core[i].inst_window_reads = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "inst_window_writes") ==
                                0) {
                                sys.core[i].inst_window_writes = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat"
                                                         "inst_window_wakeup_accesses")) {
                                sys.core[i].inst_window_wakeup_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "inst_window_selections") ==
                                0) {
                                sys.core[i].inst_window_selections = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "fp_inst_window_reads") ==
                                0) {
                                sys.core[i].fp_inst_window_reads = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "fp_inst_window_writes") ==
                                0) {
                                sys.core[i].fp_inst_window_writes = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat"
                                                         "fp_inst_window_wakeup_accesses")) {
                                sys.core[i].fp_inst_window_wakeup_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat"
                                                         "archi_int_regfile_reads")) {
                                sys.core[i].archi_int_regfile_reads = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat"
                                                         "archi_float_regfile_reads")) {
                                sys.core[i].archi_float_regfile_reads = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "phy_int_regfile_reads") ==
                                0) {
                                sys.core[i].phy_int_regfile_reads = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat"
                                                         "phy_float_regfile_reads")) {
                                sys.core[i].phy_float_regfile_reads = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "phy_int_regfile_writes") ==
                                0) {
                                sys.core[i].archi_int_regfile_writes = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat"
                                                         "phy_float_regfile_writes")) {
                                sys.core[i].archi_float_regfile_writes = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat"
                                                         "archi_int_regfile_writes")) {
                                sys.core[i].phy_int_regfile_writes = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat"
                                                         "archi_float_regfile_writes")) {
                                sys.core[i].phy_float_regfile_writes = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "int_regfile_reads")) {
                                sys.core[i].int_regfile_reads = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "float_regfile_reads") ==
                                0) {
                                sys.core[i].float_regfile_reads = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "int_regfile_writes") ==
                                0) {
                                sys.core[i].int_regfile_writes = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "float_regfile_writes") ==
                                0) {
                                sys.core[i].float_regfile_writes = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "windowed_reg_accesses") ==
                                0) {
                                sys.core[i].windowed_reg_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat"
                                                         "windowed_reg_transports")) {
                                sys.core[i].windowed_reg_transports = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "function_calls")) {
                                sys.core[i].function_calls = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "context_switches")) {
                                sys.core[i].context_switches = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "ialu_accesses")) {
                                sys.core[i].ialu_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "fpu_accesses")) {
                                sys.core[i].fpu_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "mul_accesses")) {
                                sys.core[i].mul_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "cdb_alu_accesses")) {
                                sys.core[i].cdb_alu_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "cdb_mul_accesses")) {
                                sys.core[i].cdb_mul_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "cdb_fpu_accesses")) {
                                sys.core[i].cdb_fpu_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "load_buffer_reads")) {
                                sys.core[i].load_buffer_reads = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "load_buffer_writes") ==
                                0) {
                                sys.core[i].load_buffer_writes = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "load_buffer_cams")) {
                                sys.core[i].load_buffer_cams = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "store_buffer_reads") ==
                                0) {
                                sys.core[i].store_buffer_reads = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "store_buffer_writes") ==
                                0) {
                                sys.core[i].store_buffer_writes = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "store_buffer_cams")) {
                                sys.core[i].store_buffer_cams = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "store_buffer_forwards") ==
                                0) {
                                sys.core[i].store_buffer_forwards = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "main_memory_access") ==
                                0) {
                                sys.core[i].main_memory_access = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "main_memory_read")) {
                                sys.core[i].main_memory_read = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "main_memory_write")) {
                                sys.core[i].main_memory_write = (int) std::strtol(
                                        xNode3.getChildNode("stat", k).getAttribute("value"), nullptr, 0);
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "pipeline_duty_cycle") ==
                                0) {
                                sys.core[i].pipeline_duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }

                            if (compareString(xNode3, k, "stat" "IFU_duty_cycle")) {
                                sys.core[i].IFU_duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "BR_duty_cycle")) {
                                sys.core[i].BR_duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "LSU_duty_cycle")) {
                                sys.core[i].LSU_duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "MemManU_I_duty_cycle") ==
                                0) {
                                sys.core[i].MemManU_I_duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "MemManU_D_duty_cycle") ==
                                0) {
                                sys.core[i].MemManU_D_duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "ALU_duty_cycle")) {
                                sys.core[i].ALU_duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "MUL_duty_cycle")) {
                                sys.core[i].MUL_duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "FPU_duty_cycle")) {
                                sys.core[i].FPU_duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "ALU_cdb_duty_cycle")) {
                                sys.core[i].ALU_cdb_duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "MUL_cdb_duty_cycle")) {
                                sys.core[i].MUL_cdb_duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "FPU_cdb_duty_cycle")
                                ) {
                                sys.core[i].FPU_cdb_duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                        }
                    }

                    NumofCom_4 = xNode3.nChildNode("component"); //get the number of components within the third layer
                    for (unsigned int j = 0; j < NumofCom_4; j++) {
                        xNode4 = xNode3.getChildNode("component", j);
                        if (strcmp(xNode4.getAttribute("name"), "PBT") == 0) { //find PBT
                            itmp = xNode4.nChildNode("param");
                            for (unsigned int k = 0;
                                 k < itmp; k++) { //get all items of param in system.core0.predictor--PBT
                                if (compareString(xNode4, k, "prediction_width")) {
                                    sys.core[i].predictor.prediction_width = getNodeData(xNode4, k);
                                    continue;
                                }
                                if (compareString(xNode4, k, "prediction_scheme")) {
                                    strcpy(sys.core[i].predictor.prediction_scheme,
                                           xNode4.getChildNode("param", k).getAttribute("value"));
                                    continue;
                                }
                                if (compareString(xNode4, k, "predictor_size")) {
                                    sys.core[i].predictor.predictor_size = getNodeData(xNode4, k);
                                    continue;
                                }
                                if (compareString(xNode4, k, "predictor_entries")) {
                                    sys.core[i].predictor.predictor_entries = getNodeData(xNode4, k);
                                    continue;
                                }
                                if (compareString(xNode4, k,
                                                  "local_predictor_size")) {
                                    strtmp.assign(xNode4.getChildNode("param", k).getAttribute("value"));
                                    m = 0;
                                    for (n = 0; n < strtmp.length(); n++) {
                                        if (strtmp[n] != ',') {
                                            sprintf(chtmp, "%c", strtmp[n]);
                                            strcat(chtmp1, chtmp);
                                        } else {
                                            sys.core[i].predictor.local_predictor_size[m] = (int) std::strtol(chtmp1,
                                                                                                              nullptr,
                                                                                                              0);
                                            m++;
                                            chtmp1[0] = '\0';
                                        }
                                    }
                                    sys.core[i].predictor.local_predictor_size[m] = (int) std::strtol(chtmp1, nullptr,
                                                                                                      0);
//									m++;
                                    chtmp1[0] = '\0';
                                    continue;
                                }
                                if (compareString(xNode4, k,
                                                  "local_predictor_entries")) {
                                    sys.core[i].predictor.local_predictor_entries = getNodeData(xNode4, k);
                                    continue;
                                }
                                if (compareString(xNode4, k,
                                                  "global_predictor_entries")) {
                                    sys.core[i].predictor.global_predictor_entries = getNodeData(xNode4, k);
                                    continue;
                                }
                                if (compareString(xNode4, k,
                                                  "global_predictor_bits")) {
                                    sys.core[i].predictor.global_predictor_bits = getNodeData(xNode4, k);
                                    continue;
                                }
                                if (compareString(xNode4, k,
                                                  "chooser_predictor_entries")) {
                                    sys.core[i].predictor.chooser_predictor_entries = getNodeData(xNode4, k);
                                    continue;
                                }
                                if (compareString(xNode4, k,
                                                  "chooser_predictor_bits")) {
                                    sys.core[i].predictor.chooser_predictor_bits = getNodeData(xNode4, k);
                                    continue;
                                }
                            }
                            itmp = xNode4.nChildNode("stat");
                            for (unsigned int k = 0;
                                 k < itmp; k++) { //get all items of stat in system.core0.predictor--PBT
                                if (compareString(xNode4, k, "stat", "predictor_accesses")
                                    )
                                    sys.core[i].predictor.predictor_accesses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                            }
                        }
                        if (strcmp(xNode4.getAttribute("name"), "itlb") == 0) {//find system.core0.itlb
                            itmp = xNode4.nChildNode("param");
                            for (unsigned int k = 0;
                                 k < itmp; k++) { //get all items of param in system.core0.itlb--itlb
                                if (compareString(xNode4, k, "number_entries")
                                    )
                                    sys.core[i].itlb.number_entries = getNodeData(xNode4, k);
                            }
                            itmp = xNode4.nChildNode("stat");
                            for (unsigned int k = 0; k < itmp; k++) { //get all items of stat in itlb
                                if (compareString(xNode4, k, "stat", "total_hits")) {
                                    sys.core[i].itlb.total_hits = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "total_accesses")
                                    ) {
                                    sys.core[i].itlb.total_accesses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "total_misses")) {
                                    sys.core[i].itlb.total_misses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "conflicts")) {
                                    sys.core[i].itlb.conflicts = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                            }
                        }
                        if (strcmp(xNode4.getAttribute("name"), "icache") == 0) {//find system.core0.icache
                            itmp = xNode4.nChildNode("param");
                            for (unsigned int k = 0;
                                 k < itmp; k++) { //get all items of param in system.core0.icache--icache
                                if (compareString(xNode4, k, "icache_config")
                                    ) {
                                    strtmp.assign(xNode4.getChildNode("param", k).getAttribute("value"));
                                    m = 0;
                                    for (n = 0; n < strtmp.length(); n++) {
                                        if (strtmp[n] != ',') {
                                            sprintf(chtmp, "%c", strtmp[n]);
                                            strcat(chtmp1, chtmp);
                                        } else {
                                            sys.core[i].icache.icache_config[m] = std::strtof(chtmp1, nullptr);
                                            m++;
                                            chtmp1[0] = '\0';
                                        }
                                    }
                                    sys.core[i].icache.icache_config[m] = std::strtof(chtmp1, nullptr);
//									m++;
                                    chtmp1[0] = '\0';
                                    continue;
                                }
                                if (compareString(xNode4, k, "buffer_sizes")) {
                                    strtmp.assign(xNode4.getChildNode("param", k).getAttribute("value"));
                                    m = 0;
                                    for (n = 0; n < strtmp.length(); n++) {
                                        if (strtmp[n] != ',') {
                                            sprintf(chtmp, "%c", strtmp[n]);
                                            strcat(chtmp1, chtmp);
                                        } else {
                                            sys.core[i].icache.buffer_sizes[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                            m++;
                                            chtmp1[0] = '\0';
                                        }
                                    }
                                    sys.core[i].icache.buffer_sizes[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                    //m++;
                                    chtmp1[0] = '\0';
                                }
                            }
                            itmp = xNode4.nChildNode("stat");
                            for (unsigned int k = 0; k < itmp; k++) {
                                if (compareString(xNode4, k, "stat", "total_accesses")
                                    ) {
                                    sys.core[i].icache.total_accesses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "read_accesses")) {
                                    sys.core[i].icache.read_accesses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "read_misses")) {
                                    sys.core[i].icache.read_misses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "replacements")) {
                                    sys.core[i].icache.replacements = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "read_hits")) {
                                    sys.core[i].icache.read_hits = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "total_hits")) {
                                    sys.core[i].icache.total_hits = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "total_misses")) {
                                    sys.core[i].icache.total_misses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "miss_buffer_access")
                                    ) {
                                    sys.core[i].icache.miss_buffer_access = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat",
                                                  "fill_buffer_accesses")) {
                                    sys.core[i].icache.fill_buffer_accesses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat",
                                                  "prefetch_buffer_accesses")) {
                                    sys.core[i].icache.prefetch_buffer_accesses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat",
                                                  "prefetch_buffer_writes")) {
                                    sys.core[i].icache.prefetch_buffer_writes = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat",
                                                  "prefetch_buffer_reads")) {
                                    sys.core[i].icache.prefetch_buffer_reads = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat",
                                                  "prefetch_buffer_hits")) {
                                    sys.core[i].icache.prefetch_buffer_hits = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "conflicts")) {
                                    sys.core[i].icache.conflicts = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                            }
                        }
                        if (strcmp(xNode4.getAttribute("name"), "dtlb") == 0) {//find system.core0.dtlb
                            itmp = xNode4.nChildNode("param");
                            for (unsigned int k = 0;
                                 k < itmp; k++) { //get all items of param in system.core0.dtlb--dtlb
                                if (compareString(xNode4, k, "number_entries")
                                    )
                                    sys.core[i].dtlb.number_entries = getNodeData(xNode4, k);
                            }
                            itmp = xNode4.nChildNode("stat");
                            for (unsigned int k = 0; k < itmp; k++) { //get all items of stat in dtlb
                                if (compareString(xNode4, k, "stat", "total_accesses")
                                    ) {
                                    sys.core[i].dtlb.total_accesses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "read_accesses")) {
                                    sys.core[i].dtlb.read_accesses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "write_accesses")
                                    ) {
                                    sys.core[i].dtlb.write_accesses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "read_hits")) {
                                    sys.core[i].dtlb.read_hits = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "write_hits")) {
                                    sys.core[i].dtlb.write_hits = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "read_misses")) {
                                    sys.core[i].dtlb.read_misses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "write_misses")) {
                                    sys.core[i].dtlb.write_misses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "total_hits")) {
                                    sys.core[i].dtlb.total_hits = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "total_misses")) {
                                    sys.core[i].dtlb.total_misses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "conflicts")) {
                                    sys.core[i].dtlb.conflicts = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }

                            }
                        }
                        if (strcmp(xNode4.getAttribute("name"), "dcache") == 0) {//find system.core0.dcache
                            itmp = xNode4.nChildNode("param");
                            for (unsigned int k = 0;
                                 k < itmp; k++) { //get all items of param in system.core0.dcache--dcache
                                if (compareString(xNode4, k, "dcache_config")
                                    ) {
                                    strtmp.assign(xNode4.getChildNode("param", k).getAttribute("value"));
                                    m = 0;
                                    for (n = 0; n < strtmp.length(); n++) {
                                        if (strtmp[n] != ',') {
                                            sprintf(chtmp, "%c", strtmp[n]);
                                            strcat(chtmp1, chtmp);
                                        } else {
                                            sys.core[i].dcache.dcache_config[m] = std::strtof(chtmp1, nullptr);
                                            m++;
                                            chtmp1[0] = '\0';
                                        }
                                    }
                                    sys.core[i].dcache.dcache_config[m] = std::strtof(chtmp1, nullptr);
//									m++;
                                    chtmp1[0] = '\0';
                                    continue;
                                }
                                if (compareString(xNode4, k, "buffer_sizes")) {
                                    strtmp.assign(xNode4.getChildNode("param", k).getAttribute("value"));
                                    m = 0;
                                    for (n = 0; n < strtmp.length(); n++) {
                                        if (strtmp[n] != ',') {
                                            sprintf(chtmp, "%c", strtmp[n]);
                                            strcat(chtmp1, chtmp);
                                        } else {
                                            sys.core[i].dcache.buffer_sizes[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                            m++;
                                            chtmp1[0] = '\0';
                                        }
                                    }
                                    sys.core[i].dcache.buffer_sizes[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                    //m++;
                                    chtmp1[0] = '\0';
                                }
                            }
                            itmp = xNode4.nChildNode("stat");
                            for (unsigned int k = 0; k < itmp; k++) { //get all items of stat in dcache
                                if (compareString(xNode4, k, "stat", "total_accesses")
                                    ) {
                                    sys.core[i].dcache.total_accesses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "read_accesses")) {
                                    sys.core[i].dcache.read_accesses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "write_accesses")
                                    ) {
                                    sys.core[i].dcache.write_accesses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "total_hits")) {
                                    sys.core[i].dcache.total_hits = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "total_misses")) {
                                    sys.core[i].dcache.total_misses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "read_hits")) {
                                    sys.core[i].dcache.read_hits = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "write_hits")) {
                                    sys.core[i].dcache.write_hits = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "read_misses")) {
                                    sys.core[i].dcache.read_misses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "write_misses")) {
                                    sys.core[i].dcache.write_misses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "replacements")) {
                                    sys.core[i].dcache.replacements = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "write_backs")) {
                                    sys.core[i].dcache.write_backs = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "miss_buffer_access")
                                    ) {
                                    sys.core[i].dcache.miss_buffer_access = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat",
                                                  "fill_buffer_accesses")) {
                                    sys.core[i].dcache.fill_buffer_accesses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat",
                                                  "prefetch_buffer_accesses")) {
                                    sys.core[i].dcache.prefetch_buffer_accesses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat",
                                                  "prefetch_buffer_writes")) {
                                    sys.core[i].dcache.prefetch_buffer_writes = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat",
                                                  "prefetch_buffer_reads")) {
                                    sys.core[i].dcache.prefetch_buffer_reads = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat",
                                                  "prefetch_buffer_hits")) {
                                    sys.core[i].dcache.prefetch_buffer_hits = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "wbb_writes")) {
                                    sys.core[i].dcache.wbb_writes = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "wbb_reads")) {
                                    sys.core[i].dcache.wbb_reads = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "conflicts")) {
                                    sys.core[i].dcache.conflicts = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }

                            }
                        }
                        if (strcmp(xNode4.getAttribute("name"), "BTB") == 0) {//find system.core0.BTB
                            itmp = xNode4.nChildNode("param");
                            for (unsigned int k = 0; k < itmp; k++) { //get all items of param in system.core0.BTB--BTB
                                if (compareString(xNode4, k, "BTB_config")) {
                                    strtmp.assign(xNode4.getChildNode("param", k).getAttribute("value"));
                                    m = 0;
                                    for (n = 0; n < strtmp.length(); n++) {
                                        if (strtmp[n] != ',') {
                                            sprintf(chtmp, "%c", strtmp[n]);
                                            strcat(chtmp1, chtmp);
                                        } else {
                                            sys.core[i].BTB.BTB_config[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                            m++;
                                            chtmp1[0] = '\0';
                                        }
                                    }
                                    sys.core[i].BTB.BTB_config[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                    //m++;
                                    chtmp1[0] = '\0';
                                }
                            }
                            itmp = xNode4.nChildNode("stat");
                            for (unsigned int k = 0; k < itmp; k++) { //get all items of stat in BTB
                                if (compareString(xNode4, k, "stat", "total_accesses")
                                    ) {
                                    sys.core[i].BTB.total_accesses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "read_accesses")) {
                                    sys.core[i].BTB.read_accesses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "write_accesses")
                                    ) {
                                    sys.core[i].BTB.write_accesses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "total_hits")) {
                                    sys.core[i].BTB.total_hits = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "total_misses")) {
                                    sys.core[i].BTB.total_misses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "read_hits")) {
                                    sys.core[i].BTB.read_hits = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "write_hits")) {
                                    sys.core[i].BTB.write_hits = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "read_misses")) {
                                    sys.core[i].BTB.read_misses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "write_misses")) {
                                    sys.core[i].BTB.write_misses = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                                if (compareString(xNode4, k, "stat", "replacements")) {
                                    sys.core[i].BTB.replacements = std::strtof(
                                            xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                    continue;
                                }
                            }
                        }
                    }
                } else {
                    printf("The value of homogeneous_cores or number_of_cores is not correct!");
                    exit(0);
                }
            }
        }

        //__________________________________________Get system.Repair Core____________________________________________
        int w=OrderofComponents_3layer+1;
        for (unsigned int i = 0; i < (OrderofComponents_3layer ); i++) {
            xNode3 = xNode2.getChildNode("component", w);
            if (xNode3.isEmpty() == 1) {
                printf("The value of homogeneous_L1Directories or number_of_L1Directories is not correct!");
                exit(0);
            } else {
                if (strstr(xNode3.getAttribute("id"), "L1Directory") != nullptr) {
                    itmp = xNode3.nChildNode("param");
                    for (unsigned int k = 0; k < itmp; k++) { //get all items of param in system.L1Directory
                        if (compareString(xNode3, k, "Dir_config")) {
                            strtmp.assign(xNode3.getChildNode("param", k).getAttribute("value"));
                            m = 0;
                            for (n = 0; n < strtmp.length(); n++) {
                                if (strtmp[n] != ',') {
                                    sprintf(chtmp, "%c", strtmp[n]);
                                    strcat(chtmp1, chtmp);
                                } else {
                                    sys.L1Directory[i].Dir_config[m] = std::strtof(chtmp1, nullptr);
                                    m++;
                                    chtmp1[0] = '\0';
                                }
                            }
                            sys.L1Directory[i].Dir_config[m] = std::strtof(chtmp1, nullptr);
//							m++;
                            chtmp1[0] = '\0';
                            continue;
                        }
                        if (compareString(xNode3, k, "buffer_sizes")) {
                            strtmp.assign(xNode3.getChildNode("param", k).getAttribute("value"));
                            m = 0;
                            for (n = 0; n < strtmp.length(); n++) {
                                if (strtmp[n] != ',') {
                                    sprintf(chtmp, "%c", strtmp[n]);
                                    strcat(chtmp1, chtmp);
                                } else {
                                    sys.L1Directory[i].buffer_sizes[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                    m++;
                                    chtmp1[0] = '\0';
                                }
                            }
                            sys.L1Directory[i].buffer_sizes[m] = (int) std::strtol(chtmp1, nullptr, 0);
//							m++;
                            chtmp1[0] = '\0';
                            continue;
                        }

                        if (compareString(xNode3, k, "clockrate")) {
                            sys.L1Directory[i].clockrate = getNodeData(xNode3, k);
                            continue;
                        }
                        if (compareString(xNode3, k, "vdd")) {
                            sys.L1Directory[i].vdd = getNodeDoubleData(xNode2, k);
                            continue;
                        }
                        if (compareString(xNode3, k, "power_gating_vcc")) {
                            sys.L1Directory[i].power_gating_vcc = std::strtof(
                                    xNode3.getChildNode("param", k).getAttribute("value"),
                                    nullptr);
                            continue;
                        }

                        if (compareString(xNode3, k, "ports")) {
                            strtmp.assign(xNode3.getChildNode("param", k).getAttribute("value"));
                            m = 0;
                            for (n = 0; n < strtmp.length(); n++) {
                                if (strtmp[n] != ',') {
                                    sprintf(chtmp, "%c", strtmp[n]);
                                    strcat(chtmp1, chtmp);
                                } else {
                                    sys.L1Directory[i].ports[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                    m++;
                                    chtmp1[0] = '\0';
                                }
                            }
                            sys.L1Directory[i].ports[m] = (int) std::strtol(chtmp1, nullptr, 0);
//							m++;
                            chtmp1[0] = '\0';
                            continue;
                        }
                        if (compareString(xNode3, k, "device_type")) {
                            sys.L1Directory[i].device_type = getNodeData(xNode3, k);
                            continue;
                        }
                        if (compareString(xNode3, k, "Directory_type")) {
                            sys.L1Directory[i].Directory_type = getNodeData(xNode3, k);
                            continue;
                        }
                        if (compareString(xNode3, k, "3D_stack")) {
                            strcpy(sys.L1Directory[i].threeD_stack,
                                   xNode3.getChildNode("param", k).getAttribute("value"));
                            continue;
                        }
                    }
                    itmp = xNode3.nChildNode("stat");
                    for (unsigned int k = 0; k < itmp; k++) { //get all items of stat in system.L2directorydirectory
                        if (compareString(xNode3, k, "stat" "total_accesses")) {
                            sys.L1Directory[i].total_accesses = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                        if (compareString(xNode3, k, "stat" "read_accesses")) {
                            sys.L1Directory[i].read_accesses = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                        if (compareString(xNode3, k, "stat" "write_accesses")) {
                            sys.L1Directory[i].write_accesses = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                        if (compareString(xNode3, k, "stat" "read_misses")) {
                            sys.L1Directory[i].read_misses = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                        if (compareString(xNode3, k, "stat" "write_misses")) {
                            sys.L1Directory[i].write_misses = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                        if (compareString(xNode3, k, "stat" "conflicts")) {
                            sys.L1Directory[i].conflicts = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                        if (compareString(xNode3, k, "stat" "duty_cycle")) {
                            sys.L1Directory[i].duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                    }
                    w = w + 1;
                } else {
                    printf("The value of homogeneous_L1Directories or number_of_L1Directories is not correct!");
                    exit(0);
                }
            }
        }



        //__________________________________________Get system.L1Directory0-n____________________________________________
        int tmpOrderofComponents_3layer;
        w = OrderofComponents_3layer + 1;
        tmpOrderofComponents_3layer = OrderofComponents_3layer;
        if (sys.homogeneous_L1Directories == 1) OrderofComponents_3layer = OrderofComponents_3layer + 1;
        else OrderofComponents_3layer = OrderofComponents_3layer + sys.number_of_L1Directories;

        for (unsigned int i = 0; i < (OrderofComponents_3layer - tmpOrderofComponents_3layer); i++) {
            xNode3 = xNode2.getChildNode("component", w);
            if (xNode3.isEmpty() == 1) {
                printf("The value of homogeneous_L1Directories or number_of_L1Directories is not correct!");
                exit(0);
            } else {
                if (strstr(xNode3.getAttribute("id"), "L1Directory") != nullptr) {
                    itmp = xNode3.nChildNode("param");
                    for (unsigned int k = 0; k < itmp; k++) { //get all items of param in system.L1Directory
                        if (compareString(xNode3, k, "Dir_config")) {
                            strtmp.assign(xNode3.getChildNode("param", k).getAttribute("value"));
                            m = 0;
                            for (n = 0; n < strtmp.length(); n++) {
                                if (strtmp[n] != ',') {
                                    sprintf(chtmp, "%c", strtmp[n]);
                                    strcat(chtmp1, chtmp);
                                } else {
                                    sys.L1Directory[i].Dir_config[m] = std::strtof(chtmp1, nullptr);
                                    m++;
                                    chtmp1[0] = '\0';
                                }
                            }
                            sys.L1Directory[i].Dir_config[m] = std::strtof(chtmp1, nullptr);
//							m++;
                            chtmp1[0] = '\0';
                            continue;
                        }
                        if (compareString(xNode3, k, "buffer_sizes")) {
                            strtmp.assign(xNode3.getChildNode("param", k).getAttribute("value"));
                            m = 0;
                            for (n = 0; n < strtmp.length(); n++) {
                                if (strtmp[n] != ',') {
                                    sprintf(chtmp, "%c", strtmp[n]);
                                    strcat(chtmp1, chtmp);
                                } else {
                                    sys.L1Directory[i].buffer_sizes[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                    m++;
                                    chtmp1[0] = '\0';
                                }
                            }
                            sys.L1Directory[i].buffer_sizes[m] = (int) std::strtol(chtmp1, nullptr, 0);
//							m++;
                            chtmp1[0] = '\0';
                            continue;
                        }

                        if (compareString(xNode3, k, "clockrate")) {
                            sys.L1Directory[i].clockrate = getNodeData(xNode3, k);
                            continue;
                        }
                        if (compareString(xNode3, k, "vdd")) {
                            sys.L1Directory[i].vdd = getNodeDoubleData(xNode2, k);
                            continue;
                        }
                        if (compareString(xNode3, k, "power_gating_vcc")) {
                            sys.L1Directory[i].power_gating_vcc = std::strtof(
                                    xNode3.getChildNode("param", k).getAttribute("value"),
                                    nullptr);
                            continue;
                        }

                        if (compareString(xNode3, k, "ports")) {
                            strtmp.assign(xNode3.getChildNode("param", k).getAttribute("value"));
                            m = 0;
                            for (n = 0; n < strtmp.length(); n++) {
                                if (strtmp[n] != ',') {
                                    sprintf(chtmp, "%c", strtmp[n]);
                                    strcat(chtmp1, chtmp);
                                } else {
                                    sys.L1Directory[i].ports[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                    m++;
                                    chtmp1[0] = '\0';
                                }
                            }
                            sys.L1Directory[i].ports[m] = (int) std::strtol(chtmp1, nullptr, 0);
//							m++;
                            chtmp1[0] = '\0';
                            continue;
                        }
                        if (compareString(xNode3, k, "device_type")) {
                            sys.L1Directory[i].device_type = getNodeData(xNode3, k);
                            continue;
                        }
                        if (compareString(xNode3, k, "Directory_type")) {
                            sys.L1Directory[i].Directory_type = getNodeData(xNode3, k);
                            continue;
                        }
                        if (compareString(xNode3, k, "3D_stack")) {
                            strcpy(sys.L1Directory[i].threeD_stack,
                                   xNode3.getChildNode("param", k).getAttribute("value"));
                            continue;
                        }
                    }
                    itmp = xNode3.nChildNode("stat");
                    for (unsigned int k = 0; k < itmp; k++) { //get all items of stat in system.L2directorydirectory
                        if (compareString(xNode3, k, "stat" "total_accesses")) {
                            sys.L1Directory[i].total_accesses = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                        if (compareString(xNode3, k, "stat" "read_accesses")) {
                            sys.L1Directory[i].read_accesses = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                        if (compareString(xNode3, k, "stat" "write_accesses")) {
                            sys.L1Directory[i].write_accesses = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                        if (compareString(xNode3, k, "stat" "read_misses")) {
                            sys.L1Directory[i].read_misses = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                        if (compareString(xNode3, k, "stat" "write_misses")) {
                            sys.L1Directory[i].write_misses = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                        if (compareString(xNode3, k, "stat" "conflicts")) {
                            sys.L1Directory[i].conflicts = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                        if (compareString(xNode3, k, "stat" "duty_cycle")) {
                            sys.L1Directory[i].duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                    }
                    w = w + 1;
                } else {
                    printf("The value of homogeneous_L1Directories or number_of_L1Directories is not correct!");
                    exit(0);
                }
            }
        }

        //__________________________________________Get system.L2Directory0-n____________________________________________
        w = OrderofComponents_3layer + 1;
        tmpOrderofComponents_3layer = OrderofComponents_3layer;
        if (sys.homogeneous_L2Directories == 1) OrderofComponents_3layer = OrderofComponents_3layer + 1;
        else OrderofComponents_3layer = OrderofComponents_3layer + sys.number_of_L2Directories;

        for (unsigned int i = 0; i < (OrderofComponents_3layer - tmpOrderofComponents_3layer); i++) {
            xNode3 = xNode2.getChildNode("component", w);
            if (xNode3.isEmpty() == 1) {
                printf("The value of homogeneous_L2Directories or number_of_L2Directories is not correct!");
                exit(0);
            } else {
                if (strstr(xNode3.getAttribute("id"), "L2Directory") != nullptr) {
                    itmp = xNode3.nChildNode("param");
                    for (unsigned int k = 0; k < itmp; k++) { //get all items of param in system.L2Directory
                        if (compareString(xNode3, k, "Dir_config")) {
                            strtmp.assign(xNode3.getChildNode("param", k).getAttribute("value"));
                            m = 0;
                            for (n = 0; n < strtmp.length(); n++) {
                                if (strtmp[n] != ',') {
                                    sprintf(chtmp, "%c", strtmp[n]);
                                    strcat(chtmp1, chtmp);
                                } else {
                                    sys.L2Directory[i].Dir_config[m] = std::strtof(chtmp1, nullptr);
                                    m++;
                                    chtmp1[0] = '\0';
                                }
                            }
                            sys.L2Directory[i].Dir_config[m] = std::strtof(chtmp1, nullptr);
                            //m++;
                            chtmp1[0] = '\0';
                            continue;
                        }
                        if (compareString(xNode3, k, "buffer_sizes")) {
                            strtmp.assign(xNode3.getChildNode("param", k).getAttribute("value"));
                            m = 0;
                            for (n = 0; n < strtmp.length(); n++) {
                                if (strtmp[n] != ',') {
                                    sprintf(chtmp, "%c", strtmp[n]);
                                    strcat(chtmp1, chtmp);
                                } else {
                                    sys.L2Directory[i].buffer_sizes[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                    m++;
                                    chtmp1[0] = '\0';
                                }
                            }
                            sys.L2Directory[i].buffer_sizes[m] = (int) std::strtol(chtmp1, nullptr, 0);
                            //m++;
                            chtmp1[0] = '\0';
                            continue;
                        }

                        if (compareString(xNode3, k, "clockrate")) {
                            sys.L2Directory[i].clockrate = getNodeData(xNode3, k);
                            continue;
                        }
                        if (compareString(xNode3, k, "vdd")) {
                            sys.L2Directory[i].vdd = getNodeDoubleData(xNode3, k);
                            continue;
                        }
                        if (compareString(xNode3, k, "power_gating_vcc")) {
                            sys.L2Directory[i].power_gating_vcc = getNodeDoubleData(xNode3, k);
                            continue;
                        }

                        if (compareString(xNode3, k, "Directory_type")) {
                            sys.L2Directory[i].Directory_type = getNodeData(xNode3, k);
                            continue;
                        }
                        if (compareString(xNode3, k, "ports")) {
                            strtmp.assign(xNode3.getChildNode("param", k).getAttribute("value"));
                            m = 0;
                            for (n = 0; n < strtmp.length(); n++) {
                                if (strtmp[n] != ',') {
                                    sprintf(chtmp, "%c", strtmp[n]);
                                    strcat(chtmp1, chtmp);
                                } else {
                                    sys.L2Directory[i].ports[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                    m++;
                                    chtmp1[0] = '\0';
                                }
                            }
                            sys.L2Directory[i].ports[m] = (int) std::strtol(chtmp1, nullptr, 0);
//							m++;
                            chtmp1[0] = '\0';
                            continue;
                        }
                        if (compareString(xNode3, k, "device_type")) {
                            sys.L2Directory[i].device_type = getNodeData(xNode3, k);
                            continue;
                        }
                        if (compareString(xNode3, k, "3D_stack")) {
                            strcpy(sys.L2Directory[i].threeD_stack,
                                   xNode3.getChildNode("param", k).getAttribute("value"));
                            continue;
                        }
                    }
                    itmp = xNode3.nChildNode("stat");
                    for (unsigned int k = 0; k < itmp; k++) { //get all items of stat in system.L2directorydirectory
                        if (compareString(xNode3, k, "stat" "total_accesses")) {
                            sys.L2Directory[i].total_accesses = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                        if (compareString(xNode3, k, "stat" "read_accesses")) {
                            sys.L2Directory[i].read_accesses = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                        if (compareString(xNode3, k, "stat" "write_accesses")) {
                            sys.L2Directory[i].write_accesses = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                        if (compareString(xNode3, k, "stat" "read_misses")) {
                            sys.L2Directory[i].read_misses = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                        if (compareString(xNode3, k, "stat" "write_misses")) {
                            sys.L2Directory[i].write_misses = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                        if (compareString(xNode3, k, "stat" "conflicts")) {
                            sys.L2Directory[i].conflicts = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }
                        if (compareString(xNode3, k, "stat" "duty_cycle")) {
                            sys.L2Directory[i].duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                            continue;
                        }

                    }
                    w = w + 1;
                } else {
                    printf("The value of homogeneous_L2Directories or number_of_L2Directories is not correct!");
                    exit(0);
                }
            }
        }

        //__________________________________________Get system.L2[0..n]____________________________________________
        w = OrderofComponents_3layer + 1;
        tmpOrderofComponents_3layer = OrderofComponents_3layer;
        if (sys.homogeneous_L2s == 1) OrderofComponents_3layer = OrderofComponents_3layer + 1;
        else OrderofComponents_3layer = OrderofComponents_3layer + sys.number_of_L2s;

        for (unsigned int i = 0; i < (OrderofComponents_3layer - tmpOrderofComponents_3layer); i++) {
            xNode3 = xNode2.getChildNode("component", w);
            if (xNode3.isEmpty() == 1) {
                printf("The value of homogeneous_L2s or number_of_L2s is not correct!");
                exit(0);
            } else {
                if (strstr(xNode3.getAttribute("name"), "L2") != nullptr) {
                    { //For L20-L2i
                        //Get all params with system.L2?
                        itmp = xNode3.nChildNode("param");
                        for (unsigned int k = 0; k < itmp; k++) {
                            if (compareString(xNode3, k, "L2_config")) {
                                strtmp.assign(xNode3.getChildNode("param", k).getAttribute("value"));
                                m = 0;
                                for (n = 0; n < strtmp.length(); n++) {
                                    if (strtmp[n] != ',') {
                                        sprintf(chtmp, "%c", strtmp[n]);
                                        strcat(chtmp1, chtmp);
                                    } else {
                                        sys.L2[i].L2_config[m] = std::strtof(chtmp1, nullptr);
                                        m++;
                                        chtmp1[0] = '\0';
                                    }
                                }
                                sys.L2[i].L2_config[m] = std::strtof(chtmp1, nullptr);
                                //m++;
                                chtmp1[0] = '\0';
                                continue;
                            }
                            if (compareString(xNode3, k, "clockrate")) {
                                sys.L2[i].clockrate = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "vdd")) {
                                sys.L2[i].vdd = getNodeDoubleData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "power_gating_vcc")) {
                                sys.L2[i].power_gating_vcc = getNodeDoubleData(xNode3, k);
                                continue;
                            }

                            if (compareString(xNode3, k, "merged_dir")) {
                                sys.L2[i].merged_dir = (bool) getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "ports")) {
                                strtmp.assign(xNode3.getChildNode("param", k).getAttribute("value"));
                                m = 0;
                                for (n = 0; n < strtmp.length(); n++) {
                                    if (strtmp[n] != ',') {
                                        sprintf(chtmp, "%c", strtmp[n]);
                                        strcat(chtmp1, chtmp);
                                    } else {
                                        sys.L2[i].ports[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                        m++;
                                        chtmp1[0] = '\0';
                                    }
                                }
                                sys.L2[i].ports[m] = (int) std::strtol(chtmp1, nullptr, 0);
//								m++;
                                chtmp1[0] = '\0';
                                continue;
                            }
                            if (compareString(xNode3, k, "device_type")) {
                                sys.L2[i].device_type = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "threeD_stack")) {
                                strcpy(sys.L2[i].threeD_stack, (xNode3.getChildNode("param", k).getAttribute("value")));
                                continue;
                            }
                            if (compareString(xNode3, k, "buffer_sizes")) {
                                strtmp.assign(xNode3.getChildNode("param", k).getAttribute("value"));
                                m = 0;
                                for (n = 0; n < strtmp.length(); n++) {
                                    if (strtmp[n] != ',') {
                                        sprintf(chtmp, "%c", strtmp[n]);
                                        strcat(chtmp1, chtmp);
                                    } else {
                                        sys.L2[i].buffer_sizes[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                        m++;
                                        chtmp1[0] = '\0';
                                    }
                                }
                                sys.L2[i].buffer_sizes[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                //m++;
                                chtmp1[0] = '\0';
                                continue;
                            }
                        }
                        //Get all stats with system.L2?
                        itmp = xNode3.nChildNode("stat");
                        for (unsigned int k = 0; k < itmp; k++) {
                            if (compareString(xNode3, k, "stat" "total_accesses")) {
                                sys.L2[i].total_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "read_accesses")) {
                                sys.L2[i].read_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "write_accesses")) {
                                sys.L2[i].write_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "total_hits")) {
                                sys.L2[i].total_hits = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "total_misses")) {
                                sys.L2[i].total_misses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "read_hits")) {
                                sys.L2[i].read_hits = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "write_hits")) {
                                sys.L2[i].write_hits = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "read_misses")) {
                                sys.L2[i].read_misses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "write_misses")) {
                                sys.L2[i].write_misses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "replacements")) {
                                sys.L2[i].replacements = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "write_backs")) {
                                sys.L2[i].write_backs = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "miss_buffer_accesses")
                                ) {
                                sys.L2[i].miss_buffer_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "fill_buffer_accesses")
                                ) {
                                sys.L2[i].fill_buffer_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat"
                                                         "prefetch_buffer_accesses")) {
                                sys.L2[i].prefetch_buffer_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "prefetch_buffer_writes")
                                ) {
                                sys.L2[i].prefetch_buffer_writes = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "prefetch_buffer_reads")
                                ) {
                                sys.L2[i].prefetch_buffer_reads = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "prefetch_buffer_hits")
                                ) {
                                sys.L2[i].prefetch_buffer_hits = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "wbb_writes")) {
                                sys.L2[i].wbb_writes = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "wbb_reads")) {
                                sys.L2[i].wbb_reads = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "conflicts")) {
                                sys.L2[i].conflicts = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "duty_cycle")) {
                                sys.L2[i].duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }

                            if (compareString(xNode3, k, "stat" "homenode_read_accesses")
                                ) {
                                sys.L2[i].homenode_read_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "homenode_read_accesses")
                                ) {
                                sys.L2[i].homenode_read_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "homenode_read_hits")
                                ) {
                                sys.L2[i].homenode_read_hits = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "homenode_write_hits")
                                ) {
                                sys.L2[i].homenode_write_hits = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "homenode_read_misses")
                                ) {
                                sys.L2[i].homenode_read_misses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "homenode_write_misses")
                                ) {
                                sys.L2[i].homenode_write_misses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "dir_duty_cycle")) {
                                sys.L2[i].dir_duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }

                        }
                    }
                    w = w + 1;
                } else {
                    printf("The value of homogeneous_L2s or number_of_L2s is not correct!");
                    exit(0);
                }
            }
        }
        //__________________________________________Get system.L3[0..n]____________________________________________
        w = OrderofComponents_3layer + 1;
        tmpOrderofComponents_3layer = OrderofComponents_3layer;
        if (sys.homogeneous_L3s == 1) OrderofComponents_3layer = OrderofComponents_3layer + 1;
        else OrderofComponents_3layer = OrderofComponents_3layer + sys.number_of_L3s;

        for (unsigned int i = 0; i < (OrderofComponents_3layer - tmpOrderofComponents_3layer); i++) {
            xNode3 = xNode2.getChildNode("component", w);
            if (xNode3.isEmpty() == 1) {
                printf("The value of homogeneous_L3s or number_of_L3s is not correct!");
                exit(0);
            } else {
                if (strstr(xNode3.getAttribute("name"), "L3") != nullptr) {
                    { //For L30-L3i
                        //Get all params with system.L3?
                        itmp = xNode3.nChildNode("param");
                        for (unsigned int k = 0; k < itmp; k++) {
                            if (compareString(xNode3, k, "L3_config")) {
                                strtmp.assign(xNode3.getChildNode("param", k).getAttribute("value"));
                                m = 0;
                                for (n = 0; n < strtmp.length(); n++) {
                                    if (strtmp[n] != ',') {
                                        sprintf(chtmp, "%c", strtmp[n]);
                                        strcat(chtmp1, chtmp);
                                    } else {
                                        sys.L3[i].L3_config[m] = std::strtof(chtmp1, nullptr);
                                        m++;
                                        chtmp1[0] = '\0';
                                    }
                                }
                                sys.L3[i].L3_config[m] = std::strtof(chtmp1, nullptr);
//								m++;
                                chtmp1[0] = '\0';
                                continue;
                            }
                            if (compareString(xNode3, k, "clockrate")) {
                                sys.L3[i].clockrate = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "vdd")) {
                                sys.L3[i].vdd = getNodeDoubleData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "power_gating_vcc")) {
                                sys.L3[i].power_gating_vcc = getNodeDoubleData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "merged_dir")) {
                                sys.L3[i].merged_dir = (bool) getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "ports")) {
                                strtmp.assign(xNode3.getChildNode("param", k).getAttribute("value"));
                                m = 0;
                                for (n = 0; n < strtmp.length(); n++) {
                                    if (strtmp[n] != ',') {
                                        sprintf(chtmp, "%c", strtmp[n]);
                                        strcat(chtmp1, chtmp);
                                    } else {
                                        sys.L3[i].ports[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                        m++;
                                        chtmp1[0] = '\0';
                                    }
                                }
                                sys.L3[i].ports[m] = (int) std::strtol(chtmp1, nullptr, 0);
//								m++;
                                chtmp1[0] = '\0';
                                continue;
                            }
                            if (compareString(xNode3, k, "device_type")) {
                                sys.L3[i].device_type = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "threeD_stack")) {
                                strcpy(sys.L3[i].threeD_stack, (xNode3.getChildNode("param", k).getAttribute("value")));
                                continue;
                            }
                            if (compareString(xNode3, k, "buffer_sizes")) {
                                strtmp.assign(xNode3.getChildNode("param", k).getAttribute("value"));
                                m = 0;
                                for (n = 0; n < strtmp.length(); n++) {
                                    if (strtmp[n] != ',') {
                                        sprintf(chtmp, "%c", strtmp[n]);
                                        strcat(chtmp1, chtmp);
                                    } else {
                                        sys.L3[i].buffer_sizes[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                        m++;
                                        chtmp1[0] = '\0';
                                    }
                                }
                                sys.L3[i].buffer_sizes[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                //m++;
                                chtmp1[0] = '\0';
                                continue;
                            }
                        }
                        //Get all stats with system.L3?
                        itmp = xNode3.nChildNode("stat");
                        for (unsigned int k = 0; k < itmp; k++) {
                            if (compareString(xNode3, k, "stat" "total_accesses")) {
                                sys.L3[i].total_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "read_accesses")) {
                                sys.L3[i].read_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "write_accesses")) {
                                sys.L3[i].write_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "total_hits")) {
                                sys.L3[i].total_hits = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "total_misses")) {
                                sys.L3[i].total_misses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "read_hits")) {
                                sys.L3[i].read_hits = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "write_hits")) {
                                sys.L3[i].write_hits = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "read_misses")) {
                                sys.L3[i].read_misses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "write_misses")) {
                                sys.L3[i].write_misses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "replacements")) {
                                sys.L3[i].replacements = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "write_backs")) {
                                sys.L3[i].write_backs = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "miss_buffer_accesses")
                                ) {
                                sys.L3[i].miss_buffer_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "fill_buffer_accesses")
                                ) {
                                sys.L3[i].fill_buffer_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat"
                                                         "prefetch_buffer_accesses")) {
                                sys.L3[i].prefetch_buffer_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "prefetch_buffer_writes")
                                ) {
                                sys.L3[i].prefetch_buffer_writes = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "prefetch_buffer_reads")
                                ) {
                                sys.L3[i].prefetch_buffer_reads = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "prefetch_buffer_hits")
                                ) {
                                sys.L3[i].prefetch_buffer_hits = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "wbb_writes")) {
                                sys.L3[i].wbb_writes = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "wbb_reads")) {
                                sys.L3[i].wbb_reads = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "conflicts")) {
                                sys.L3[i].conflicts = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "duty_cycle")) {
                                sys.L3[i].duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }

                            if (compareString(xNode3, k, "stat" "homenode_read_accesses")
                                ) {
                                sys.L3[i].homenode_read_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "homenode_read_accesses")
                                ) {
                                sys.L3[i].homenode_read_accesses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "homenode_read_hits")
                                ) {
                                sys.L3[i].homenode_read_hits = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "homenode_write_hits")
                                ) {
                                sys.L3[i].homenode_write_hits = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "homenode_read_misses")
                                ) {
                                sys.L3[i].homenode_read_misses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "homenode_write_misses")
                                ) {
                                sys.L3[i].homenode_write_misses = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }
                            if (compareString(xNode3, k, "stat" "dir_duty_cycle")) {
                                sys.L3[i].dir_duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                                continue;
                            }

                        }
                    }
                    w = w + 1;
                } else {
                    printf("The value of homogeneous_L3s or number_of_L3s is not correct!");
                    exit(0);
                }
            }
        }
        //__________________________________________Get system.NoC[0..n]____________________________________________
        w = OrderofComponents_3layer + 1;
        tmpOrderofComponents_3layer = OrderofComponents_3layer;
        if (sys.homogeneous_NoCs == 1) OrderofComponents_3layer = OrderofComponents_3layer + 1;
        else OrderofComponents_3layer = OrderofComponents_3layer + sys.number_of_NoCs;

        for (unsigned int i = 0; i < (OrderofComponents_3layer - tmpOrderofComponents_3layer); i++) {
            xNode3 = xNode2.getChildNode("component", w);
            if (xNode3.isEmpty() == 1) {
                printf("The value of homogeneous_NoCs or number_of_NoCs is not correct!");
                exit(0);
            } else {
                if (strstr(xNode3.getAttribute("name"), "noc") != nullptr) {
                    { //For NoC0-NoCi
                        //Get all params with system.NoC?
                        itmp = xNode3.nChildNode("param");
                        for (unsigned int k = 0; k < itmp; k++) {
                            if (compareString(xNode3, k, "clockrate")) {
                                sys.NoC[i].clockrate = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "type")) {
                                sys.NoC[i].type = (bool) getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "vdd")) {
                                sys.NoC[i].vdd = getNodeDoubleData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "power_gating_vcc")) {
                                sys.NoC[i].power_gating_vcc = getNodeDoubleData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "topology")) {
                                strcpy(sys.NoC[i].topology, (xNode3.getChildNode("param", k).getAttribute("value")));
                                continue;
                            }
                            if (compareString(xNode3, k, "horizontal_nodes")) {
                                sys.NoC[i].horizontal_nodes = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "vertical_nodes")) {
                                sys.NoC[i].vertical_nodes = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "has_global_link")) {
                                sys.NoC[i].has_global_link = (bool) getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "link_throughput")) {
                                sys.NoC[i].link_throughput = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "link_latency")) {
                                sys.NoC[i].link_latency = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "input_ports")) {
                                sys.NoC[i].input_ports = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "output_ports")) {
                                sys.NoC[i].output_ports = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k,
                                              "virtual_channel_per_port")) {
                                sys.NoC[i].virtual_channel_per_port = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "flit_bits")) {
                                sys.NoC[i].flit_bits = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k,
                                              "input_buffer_entries_per_vc")) {
                                sys.NoC[i].input_buffer_entries_per_vc = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "dual_pump")) {
                                sys.NoC[i].dual_pump = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "chip_coverage")) {
                                sys.NoC[i].chip_coverage = getNodeDoubleData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k,
                                              "link_routing_over_percentage")) {
                                sys.NoC[i].route_over_perc = getNodeDoubleData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "ports_of_input_buffer")
                                ) {
                                strtmp.assign(xNode3.getChildNode("param", k).getAttribute("value"));
                                m = 0;
                                for (n = 0; n < strtmp.length(); n++) {
                                    if (strtmp[n] != ',') {
                                        sprintf(chtmp, "%c", strtmp[n]);
                                        strcat(chtmp1, chtmp);
                                    } else {
                                        sys.NoC[i].ports_of_input_buffer[m] = (int) std::strtol(chtmp1, nullptr, 0);
                                        m++;
                                        chtmp1[0] = '\0';
                                    }
                                }
                                sys.NoC[i].ports_of_input_buffer[m] = (int) std::strtol(chtmp1, nullptr, 0);
//								m++;
                                chtmp1[0] = '\0';
                                continue;
                            }
                            if (compareString(xNode3, k, "number_of_crossbars")
                                ) {
                                sys.NoC[i].number_of_crossbars = getNodeData(xNode3, k);
                                continue;
                            }
                            if (compareString(xNode3, k, "crossbar_type")) {
                                strcpy(sys.NoC[i].crossbar_type,
                                       (xNode3.getChildNode("param", k).getAttribute("value")));
                                continue;
                            }
                            if (compareString(xNode3, k, "crosspoint_type")) {
                                strcpy(sys.NoC[i].crosspoint_type,
                                       (xNode3.getChildNode("param", k).getAttribute("value")));
                                continue;
                            }
                            if (compareString(xNode3, k, "arbiter_type")) {
                                sys.NoC[i].arbiter_type = getNodeData(xNode3, k);
                                continue;
                            }
                        }
                        NumofCom_4 = xNode3.nChildNode(
                                "component"); //get the number of components within the third layer
                        for (unsigned int j = 0; j < NumofCom_4; j++) {
                            xNode4 = xNode3.getChildNode("component", j);
                            if (strcmp(xNode4.getAttribute("name"), "xbar0") == 0) { //find PBT
                                itmp = xNode4.nChildNode("param");
                                for (unsigned int k = 0;
                                     k < itmp; k++) { //get all items of param in system.XoC0.xbar0--xbar0
                                    if (compareString(xNode4, k,
                                                      "number_of_inputs_of_crossbars")) {
                                        sys.NoC[i].xbar0.number_of_inputs_of_crossbars = getNodeData(xNode4, k);
                                        continue;
                                    }
                                    if (compareString(xNode4, k,
                                                      "number_of_outputs_of_crossbars")) {
                                        sys.NoC[i].xbar0.number_of_outputs_of_crossbars = getNodeData(xNode4, k);
                                        continue;
                                    }
                                    if (compareString(xNode4, k, "flit_bits")
                                        ) {
                                        sys.NoC[i].xbar0.flit_bits = getNodeData(xNode4, k);
                                        continue;
                                    }
                                    if (compareString(xNode4, k,
                                                      "input_buffer_entries_per_port")) {
                                        sys.NoC[i].xbar0.input_buffer_entries_per_port = getNodeData(xNode4, k);
                                        continue;
                                    }
                                    if (compareString(xNode4, k,
                                                      "ports_of_input_buffer")) {
                                        strtmp.assign(xNode4.getChildNode("param", k).getAttribute("value"));
                                        m = 0;
                                        for (n = 0; n < strtmp.length(); n++) {
                                            if (strtmp[n] != ',') {
                                                sprintf(chtmp, "%c", strtmp[n]);
                                                strcat(chtmp1, chtmp);
                                            } else {
                                                sys.NoC[i].xbar0.ports_of_input_buffer[m] = (int) std::strtol(chtmp1,
                                                                                                              nullptr,
                                                                                                              0);
                                                m++;
                                                chtmp1[0] = '\0';
                                            }
                                        }
                                        sys.NoC[i].xbar0.ports_of_input_buffer[m] = (int) std::strtol(chtmp1, nullptr,
                                                                                                      0);
                                        //				m++;
                                        chtmp1[0] = '\0';
                                    }
                                }
                                itmp = xNode4.nChildNode("stat");
                                for (unsigned int k = 0;
                                     k < itmp; k++) { //get all items of stat in system.core0.predictor--PBT
                                    if (compareString(xNode4, k, "stat", "predictor_accesses"))
                                        sys.core[i].predictor.predictor_accesses = std::strtof(
                                                xNode4.getChildNode("stat", k).getAttribute("value"), nullptr);
                                }
                            }
                        }
                        //Get all stats with system.NoC?
                        itmp = xNode3.nChildNode("stat");
                        for (unsigned int k = 0; k < itmp; k++) {
                            if (compareString(xNode3, k, "stat" "total_accesses"))
                                sys.NoC[i].total_accesses = getNodeDoubleData(xNode3, k, "stat");
                            if (compareString(xNode3, k, "stat" "duty_cycle"))
                                sys.NoC[i].duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                        }
                    }
                    w = w + 1;
                }
            }
        }
        //__________________________________________Get system.mem____________________________________________
//		if (OrderofComponents_3layer>0) OrderofComponents_3layer=OrderofComponents_3layer+1;
//		xNode3=xNode2.getChildNode("component",OrderofComponents_3layer);
//		if (xNode3.isEmpty()==1) {
//			printf("some value(s) of number_of_cores/number_of_L2s/number_of_L3s/number_of_NoCs is/are not correct!");
//			exit(0);
//		}
//		if (strstr(xNode3.getAttribute("id"),"system.mem")!=nullptr)
//		{
//
//			itmp=xNode3.nChildNode("param");
//			for(unsigned int k=0; k<itmp; k++)
//			{ //get all items of param in system.mem
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"mem_tech_node")==0) {sys.mem.mem_tech_node=getNodeData(xNode3,k);continue;}
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"device_clock")==0) {sys.mem.device_clock=getNodeData(xNode3,k);continue;}
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"peak_transfer_rate")==0) {sys.mem.peak_transfer_rate=getNodeData(xNode3,k);continue;}
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"capacity_per_channel")==0) {sys.mem.capacity_per_channel=getNodeData(xNode3,k);continue;}
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"number_ranks")==0) {sys.mem.number_ranks=getNodeData(xNode3,k);continue;}
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"num_banks_of_DRAM_chip")==0) {sys.mem.num_banks_of_DRAM_chip=getNodeData(xNode3,k);continue;}
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"Block_width_of_DRAM_chip")==0) {sys.mem.Block_width_of_DRAM_chip=getNodeData(xNode3,k);continue;}
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"output_width_of_DRAM_chip")==0) {sys.mem.output_width_of_DRAM_chip=getNodeData(xNode3,k);continue;}
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"page_size_of_DRAM_chip")==0) {sys.mem.page_size_of_DRAM_chip=getNodeData(xNode3,k);continue;}
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"burstlength_of_DRAM_chip")==0) {sys.mem.burstlength_of_DRAM_chip=getNodeData(xNode3,k);continue;}
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"internal_prefetch_of_DRAM_chip")==0) {sys.mem.internal_prefetch_of_DRAM_chip=getNodeData(xNode3,k);continue;}
//			}
//			itmp=xNode3.nChildNode("stat");
//			for(unsigned int k=0; k<itmp; k++)
//			{ //get all items of stat in system.mem
//				if (strcmp(xNode3.getChildNode("stat",k).getAttribute("name"),"memory_accesses")==0) {sys.mem.memory_accesses=getNodeDoubleData(xNode3,k,"stat");continue;}
//				if (strcmp(xNode3.getChildNode("stat",k).getAttribute("name"),"memory_reads")==0) {sys.mem.memory_reads=getNodeDoubleData(xNode3,k,"stat");continue;}
//				if (strcmp(xNode3.getChildNode("stat",k).getAttribute("name"),"memory_writes")==0) {sys.mem.memory_writes=getNodeDoubleData(xNode3,k,"stat");continue;}
//			}
//		}
//		else{
//			printf("some value(s) of number_of_cores/number_of_L2s/number_of_L3s/number_of_NoCs is/are not correct!");
//			exit(0);
//		}
        //__________________________________________Get system.mc____________________________________________
        if (OrderofComponents_3layer > 0) OrderofComponents_3layer = OrderofComponents_3layer + 1;
        xNode3 = xNode2.getChildNode("component", OrderofComponents_3layer);
        if (xNode3.isEmpty() == 1) {
            printf("some value(s) of number_of_cores/number_of_L2s/number_of_L3s/number_of_NoCs is/are not correct!");
            exit(0);
        }
        if (strstr(xNode3.getAttribute("id"), "system.mc") != nullptr) {
            itmp = xNode3.nChildNode("param");
            for (unsigned int k = 0; k < itmp; k++) { //get all items of param in system.mem
                if (compareString(xNode3, k, "mc_clock")) {
                    sys.mc.mc_clock = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "vdd")) {
                    sys.mc.vdd = getNodeDoubleData(xNode2, k);
                    continue;
                }
                if (compareString(xNode3, k, "power_gating_vcc")) {
                    sys.mc.power_gating_vcc = getNodeDoubleData(xNode2, k);
                    continue;
                }
                if (compareString(xNode3, k, "block_size")) {
                    sys.mc.llc_line_length = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "number_mcs")) {
                    sys.mc.number_mcs = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "memory_channels_per_mc")) {
                    sys.mc.memory_channels_per_mc = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "req_window_size_per_channel")) {
                    sys.mc.req_window_size_per_channel = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "IO_buffer_size_per_channel")) {
                    sys.mc.IO_buffer_size_per_channel = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "databus_width")) {
                    sys.mc.databus_width = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "addressbus_width")) {
                    sys.mc.addressbus_width = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "peak_transfer_rate")) {
                    sys.mc.peak_transfer_rate = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "number_ranks")) {
                    sys.mc.number_ranks = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "LVDS")) {
                    sys.mc.LVDS = (bool) getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "type")) {
                    sys.mc.type = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "withPHY")) {
                    sys.mc.withPHY = (bool) getNodeData(xNode3, k);
                    continue;
                }

            }
            itmp = xNode3.nChildNode("stat");
            for (unsigned int k = 0; k < itmp; k++) { //get all items of stat in system.mendirectory
                if (compareString(xNode3, k, "stat" "memory_accesses")) {
                    sys.mc.memory_accesses = getNodeDoubleData(xNode3, k, "stat");
                    continue;
                }
                if (compareString(xNode3, k, "stat" "memory_reads")) {
                    sys.mc.memory_reads = getNodeDoubleData(xNode3, k, "stat");
                    continue;
                }
                if (compareString(xNode3, k, "stat" "memory_writes")) {
                    sys.mc.memory_writes = getNodeDoubleData(xNode3, k, "stat");
                    continue;
                }
            }
        } else {
            printf("some value(s) of number_of_cores/number_of_L2s/number_of_L3s/number_of_NoCs is/are not correct!");
            exit(0);
        }
        //__________________________________________Get system.niu____________________________________________
        if (OrderofComponents_3layer > 0) OrderofComponents_3layer = OrderofComponents_3layer + 1;
        xNode3 = xNode2.getChildNode("component", OrderofComponents_3layer);
        if (xNode3.isEmpty() == 1) {
            printf("some value(s) of number_of_cores/number_of_L2s/number_of_L3s/number_of_NoCs is/are not correct!");
            exit(0);
        }
        if (strstr(xNode3.getAttribute("id"), "system.niu") != nullptr) {
            itmp = xNode3.nChildNode("param");
            for (unsigned int k = 0; k < itmp; k++) { //get all items of param in system.mem
                if (compareString(xNode3, k, "clockrate")) {
                    sys.niu.clockrate = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "number_units")) {
                    sys.niu.number_units = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "type")) {
                    sys.niu.type = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "vdd")) {
                    sys.niu.vdd = getNodeDoubleData(xNode2, k);
                    continue;
                }
                if (compareString(xNode3, k, "power_gating_vcc")) {
                    sys.niu.power_gating_vcc = getNodeDoubleData(xNode2, k);
                    continue;
                }

            }
            itmp = xNode3.nChildNode("stat");
            for (unsigned int k = 0; k < itmp; k++) { //get all items of stat in system.mendirectory
                if (compareString(xNode3, k, "stat" "duty_cycle")) {
                    sys.niu.duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                    continue;
                }
                if (compareString(xNode3, k, "stat" "total_load_perc")) {
                    sys.niu.total_load_perc = getNodeDoubleData(xNode3, k, "stat");
                    continue;
                }
            }
        } else {
            printf("some value(s) of number_of_cores/number_of_L2s/number_of_L3s/number_of_NoCs is/are not correct!");
            exit(0);
        }

        //__________________________________________Get system.pcie____________________________________________
        if (OrderofComponents_3layer > 0) OrderofComponents_3layer = OrderofComponents_3layer + 1;
        xNode3 = xNode2.getChildNode("component", OrderofComponents_3layer);
        if (xNode3.isEmpty() == 1) {
            printf("some value(s) of number_of_cores/number_of_L2s/number_of_L3s/number_of_NoCs is/are not correct!");
            exit(0);
        }
        if (strstr(xNode3.getAttribute("id"), "system.pcie") != nullptr) {
            itmp = xNode3.nChildNode("param");
            for (unsigned int k = 0; k < itmp; k++) { //get all items of param in system.mem
                if (compareString(xNode3, k, "clockrate")) {
                    sys.pcie.clockrate = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "number_units")) {
                    sys.pcie.number_units = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "num_channels")) {
                    sys.pcie.num_channels = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "type")) {
                    sys.pcie.type = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "vdd")) {
                    sys.pcie.vdd = getNodeDoubleData(xNode2, k);
                    continue;
                }
                if (compareString(xNode3, k, "power_gating_vcc")) {
                    sys.pcie.power_gating_vcc = getNodeDoubleData(xNode2, k);
                    continue;
                }
                if (compareString(xNode3, k, "withPHY")) {
                    sys.pcie.withPHY = (bool) getNodeData(xNode3, k);
                    continue;
                }

            }
            itmp = xNode3.nChildNode("stat");
            for (unsigned int k = 0; k < itmp; k++) { //get all items of stat in system.mendirectory
                if (compareString(xNode3, k, "stat" "duty_cycle")) {
                    sys.pcie.duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                    continue;
                }
                if (compareString(xNode3, k, "stat" "total_load_perc")) {
                    sys.pcie.total_load_perc = getNodeDoubleData(xNode3, k, "stat");
                    continue;
                }
            }
        } else {
            printf("some value(s) of number_of_cores/number_of_L2s/number_of_L3s/number_of_NoCs is/are not correct!");
            exit(0);
        }
        //__________________________________________Get system.flashcontroller____________________________________________
        if (OrderofComponents_3layer > 0) OrderofComponents_3layer = OrderofComponents_3layer + 1;
        xNode3 = xNode2.getChildNode("component", OrderofComponents_3layer);
        if (xNode3.isEmpty() == 1) {
            printf("some value(s) of number_of_cores/number_of_L2s/number_of_L3s/number_of_NoCs is/are not correct!");
            exit(0);
        }
        if (strstr(xNode3.getAttribute("id"), "system.flashc") != nullptr) {
            itmp = xNode3.nChildNode("param");
            for (unsigned int k = 0; k < itmp; k++) { //get all items of param in system.mem
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"flashc_clock")==0) {sys.flashc.mc_clock=getNodeData(xNode3,k);continue;}
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"block_size")==0) {sys.flashc.llc_line_length=getNodeData(xNode3,k);continue;}
                if (compareString(xNode3, k, "number_flashcs")) {
                    sys.flashc.number_mcs = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "vdd")) {
                    sys.flashc.vdd = getNodeDoubleData(xNode2, k);
                    continue;
                }
                if (compareString(xNode3, k, "power_gating_vcc")) {
                    sys.flashc.power_gating_vcc = getNodeDoubleData(xNode2, k);
                    continue;
                }
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"memory_channels_per_flashc")==0) {sys.flashc.memory_channels_per_mc=getNodeData(xNode3,k);continue;}
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"req_window_size_per_channel")==0) {sys.flashc.req_window_size_per_channel=getNodeData(xNode3,k);continue;}
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"IO_buffer_size_per_channel")==0) {sys.flashc.IO_buffer_size_per_channel=getNodeData(xNode3,k);continue;}
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"databus_width")==0) {sys.flashc.databus_width=getNodeData(xNode3,k);continue;}
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"addressbus_width")==0) {sys.flashc.addressbus_width=getNodeData(xNode3,k);continue;}
                if (compareString(xNode3, k, "peak_transfer_rate")) {
                    sys.flashc.peak_transfer_rate = getNodeData(xNode3, k);
                    continue;
                }
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"number_ranks")==0) {sys.flashc.number_ranks=getNodeData(xNode3,k);continue;}
//				if (strcmp(xNode3.getChildNode("param",k).getAttribute("name"),"LVDS")==0) {sys.flashc.LVDS=(bool)getNodeData(xNode3,k);continue;}
                if (compareString(xNode3, k, "type")) {
                    sys.flashc.type = getNodeData(xNode3, k);
                    continue;
                }
                if (compareString(xNode3, k, "withPHY")) {
                    sys.flashc.withPHY = (bool) getNodeData(xNode3, k);
                    continue;
                }

            }
            itmp = xNode3.nChildNode("stat");
            for (unsigned int k = 0; k < itmp; k++) { //get all items of stat in system.mendirectory
//				if (strcmp(xNode3.getChildNode("stat",k).getAttribute("name"),"memory_accesses")==0) {sys.flashc.memory_accesses=getNodeDoubleData(xNode3,k,"stat");continue;}
//				if (strcmp(xNode3.getChildNode("stat",k).getAttribute("name"),"memory_reads")==0) {sys.flashc.memory_reads=getNodeDoubleData(xNode3,k,"stat");continue;}
//				if (strcmp(xNode3.getChildNode("stat",k).getAttribute("name"),"memory_writes")==0) {sys.flashc.memory_writes=getNodeDoubleData(xNode3,k,"stat");continue;}
                if (compareString(xNode3, k, "stat" "duty_cycle")) {
                    sys.flashc.duty_cycle = getNodeDoubleData(xNode3, k, "stat");
                    continue;
                }
                if (compareString(xNode3, k, "stat" "total_load_perc")) {
                    sys.flashc.total_load_perc = getNodeDoubleData(xNode3, k, "stat");
                    continue;
                }

            }
        } else {
            printf("some value(s) of number_of_cores/number_of_L2s/number_of_L3s/number_of_NoCs is/are not correct!");
            exit(0);
        }

    }
}

void ParseXML::initialize() //Initialize all
{
    //All number_of_* at the level of 'system' 03/21/2009
    sys.number_of_cores = 1;
    sys.number_of_L1Directories = 1;
    sys.number_of_L2Directories = 1;
    sys.number_of_L2s = 1;
    sys.Private_L2 = false;
    sys.number_of_L3s = 1;
    sys.number_of_NoCs = 1;
    // All params at the level of 'system'
    //strcpy(sys.homogeneous_cores,"default");
    sys.core_tech_node = 1;
    sys.target_core_clockrate = 1;
    sys.target_chip_area = 1;
    sys.temperature = 360;
    sys.number_cache_levels = 1;
    sys.homogeneous_cores = 1;
    sys.homogeneous_L1Directories = 1;
    sys.homogeneous_L2Directories = 1;
    sys.homogeneous_L2s = 1;
    sys.homogeneous_L3s = 1;
    sys.homogeneous_NoCs = 1;
    sys.homogeneous_ccs = 1;

    sys.Max_area_deviation = 1;
    sys.Max_power_deviation = 1;
    sys.device_type = 1;
    sys.longer_channel_device = true;
    sys.power_gating = false;
    sys.Embedded = false;
    sys.opt_dynamic_power = false;
    sys.opt_lakage_power = false;
    sys.opt_clockrate = true;
    sys.opt_area = false;
    sys.interconnect_projection_type = 1;
    sys.vdd = 0;
    sys.power_gating_vcc = -1;
    for (unsigned int i = 0; i <= 63; i++) {
        sys.core[i].vdd = 0;
        sys.core[i].power_gating_vcc = -1;
        sys.core[i].clock_rate = 1;
        sys.core[i].opt_local = true;
        sys.core[i].x86 = false;
        sys.core[i].machine_bits = 1;
        sys.core[i].virtual_address_width = 1;
        sys.core[i].physical_address_width = 1;
        sys.core[i].opcode_width = 1;
        sys.core[i].micro_opcode_width = 1;
        //strcpy(sys.core[i].machine_type,"default");
        sys.core[i].internal_datapath_width = 1;
        sys.core[i].number_hardware_threads = 1;
        sys.core[i].fetch_width = 1;
        sys.core[i].number_instruction_fetch_ports = 1;
        sys.core[i].decode_width = 1;
        sys.core[i].issue_width = 1;
        sys.core[i].peak_issue_width = 1;
        sys.core[i].commit_width = 1;
        for (unsigned int j = 0; j < 20; j++) {
            sys.core[i].pipelines_per_core[j] = 1;
            sys.core[i].pipeline_depth[j] = 1;
        }
        strcpy(sys.core[i].FPU, "default");
        strcpy(sys.core[i].divider_multiplier, "default");
        sys.core[i].ALU_per_core = 1;
        sys.core[i].FPU_per_core = 1.0;
        sys.core[i].MUL_per_core = 1;
        sys.core[i].instruction_buffer_size = 1;
        sys.core[i].decoded_stream_buffer_size = 1;
        //strcpy(sys.core[i].instruction_window_scheme,"default");
        sys.core[i].instruction_window_size = 1;
        sys.core[i].ROB_size = 1;
        sys.core[i].archi_Regs_IRF_size = 1;
        sys.core[i].archi_Regs_FRF_size = 1;
        sys.core[i].phy_Regs_IRF_size = 1;
        sys.core[i].phy_Regs_FRF_size = 1;
        //strcpy(sys.core[i].rename_scheme,"default");
        sys.core[i].checkpoint_depth = 1;
        sys.core[i].register_windows_size = 1;
        strcpy(sys.core[i].LSU_order, "default");
        sys.core[i].store_buffer_size = 1;
        sys.core[i].load_buffer_size = 1;
        sys.core[i].memory_ports = 1;
        strcpy(sys.core[i].Dcache_dual_pump, "default");
        sys.core[i].RAS_size = 1;
        //all stats at the level of system.core(0-n)
        sys.core[i].total_instructions = 1;
        sys.core[i].int_instructions = 1;
        sys.core[i].fp_instructions = 1;
        sys.core[i].branch_instructions = 1;
        sys.core[i].branch_mispredictions = 1;
        sys.core[i].committed_instructions = 1;
        sys.core[i].load_instructions = 1;
        sys.core[i].store_instructions = 1;
        sys.core[i].total_cycles = 1;
        sys.core[i].idle_cycles = 1;
        sys.core[i].busy_cycles = 1;
        sys.core[i].instruction_buffer_reads = 1;
        sys.core[i].instruction_buffer_write = 1;
        sys.core[i].ROB_reads = 1;
        sys.core[i].ROB_writes = 1;
        sys.core[i].rename_accesses = 1;
        sys.core[i].inst_window_reads = 1;
        sys.core[i].inst_window_writes = 1;
        sys.core[i].inst_window_wakeup_accesses = 1;
        sys.core[i].inst_window_selections = 1;
        sys.core[i].archi_int_regfile_reads = 1;
        sys.core[i].archi_float_regfile_reads = 1;
        sys.core[i].phy_int_regfile_reads = 1;
        sys.core[i].phy_float_regfile_reads = 1;
        sys.core[i].windowed_reg_accesses = 1;
        sys.core[i].windowed_reg_transports = 1;
        sys.core[i].function_calls = 1;
        sys.core[i].ialu_accesses = 1;
        sys.core[i].fpu_accesses = 1;
        sys.core[i].mul_accesses = 1;
        sys.core[i].cdb_alu_accesses = 1;
        sys.core[i].cdb_mul_accesses = 1;
        sys.core[i].cdb_fpu_accesses = 1;
        sys.core[i].load_buffer_reads = 1;
        sys.core[i].load_buffer_writes = 1;
        sys.core[i].load_buffer_cams = 1;
        sys.core[i].store_buffer_reads = 1;
        sys.core[i].store_buffer_writes = 1;
        sys.core[i].store_buffer_cams = 1;
        sys.core[i].store_buffer_forwards = 1;
        sys.core[i].main_memory_access = 1;
        sys.core[i].main_memory_read = 1;
        sys.core[i].main_memory_write = 1;
        sys.core[i].IFU_duty_cycle = 1;
        sys.core[i].BR_duty_cycle = 1;
        sys.core[i].LSU_duty_cycle = 1;
        sys.core[i].MemManU_I_duty_cycle = 1;
        sys.core[i].MemManU_D_duty_cycle = 1;
        sys.core[i].ALU_duty_cycle = 1;
        sys.core[i].MUL_duty_cycle = 1;
        sys.core[i].FPU_duty_cycle = 1;
        sys.core[i].ALU_cdb_duty_cycle = 1;
        sys.core[i].MUL_cdb_duty_cycle = 1;
        sys.core[i].FPU_cdb_duty_cycle = 1;
        sys.core[i].instructionPredictor.predictor_bits = 100;
        sys.core[i].instructionPredictor.predictor_entries = 100;
        //system.core?.predictor
        sys.core[i].predictor.prediction_width = 1;
        strcpy(sys.core[i].predictor.prediction_scheme, "default");
        sys.core[i].predictor.predictor_size = 1;
        sys.core[i].predictor.predictor_entries = 1;
        sys.core[i].predictor.local_predictor_entries = 1;
        for (unsigned int j = 0; j < 20; j++) sys.core[i].predictor.local_predictor_size[j] = 1;
        sys.core[i].predictor.global_predictor_entries = 1;
        sys.core[i].predictor.global_predictor_bits = 1;
        sys.core[i].predictor.chooser_predictor_entries = 1;
        sys.core[i].predictor.chooser_predictor_bits = 1;
        sys.core[i].predictor.predictor_accesses = 1;
        //system.core?.itlb
        sys.core[i].itlb.number_entries = 1;
        sys.core[i].itlb.total_hits = 1;
        sys.core[i].itlb.total_accesses = 1;
        sys.core[i].itlb.total_misses = 1;
        //system.core?.icache
        for (unsigned int j = 0; j < 20; j++) sys.core[i].icache.icache_config[j] = 1;
        //strcpy(sys.core[i].icache.buffer_sizes,"default");
        sys.core[i].icache.total_accesses = 1;
        sys.core[i].icache.read_accesses = 1;
        sys.core[i].icache.read_misses = 1;
        sys.core[i].icache.replacements = 1;
        sys.core[i].icache.read_hits = 1;
        sys.core[i].icache.total_hits = 1;
        sys.core[i].icache.total_misses = 1;
        sys.core[i].icache.miss_buffer_access = 1;
        sys.core[i].icache.fill_buffer_accesses = 1;
        sys.core[i].icache.prefetch_buffer_accesses = 1;
        sys.core[i].icache.prefetch_buffer_writes = 1;
        sys.core[i].icache.prefetch_buffer_reads = 1;
        sys.core[i].icache.prefetch_buffer_hits = 1;
        //system.core?.dtlb
        sys.core[i].dtlb.number_entries = 1;
        sys.core[i].dtlb.total_accesses = 1;
        sys.core[i].dtlb.read_accesses = 1;
        sys.core[i].dtlb.write_accesses = 1;
        sys.core[i].dtlb.write_hits = 1;
        sys.core[i].dtlb.read_hits = 1;
        sys.core[i].dtlb.read_misses = 1;
        sys.core[i].dtlb.write_misses = 1;
        sys.core[i].dtlb.total_hits = 1;
        sys.core[i].dtlb.total_misses = 1;
        //system.core?.dcache
        for (unsigned int j = 0; j < 20; j++) sys.core[i].dcache.dcache_config[j] = 1;
        //strcpy(sys.core[i].dcache.buffer_sizes,"default");
        sys.core[i].dcache.total_accesses = 1;
        sys.core[i].dcache.read_accesses = 1;
        sys.core[i].dcache.write_accesses = 1;
        sys.core[i].dcache.total_hits = 1;
        sys.core[i].dcache.total_misses = 1;
        sys.core[i].dcache.read_hits = 1;
        sys.core[i].dcache.write_hits = 1;
        sys.core[i].dcache.read_misses = 1;
        sys.core[i].dcache.write_misses = 1;
        sys.core[i].dcache.replacements = 1;
        sys.core[i].dcache.write_backs = 1;
        sys.core[i].dcache.miss_buffer_access = 1;
        sys.core[i].dcache.fill_buffer_accesses = 1;
        sys.core[i].dcache.prefetch_buffer_accesses = 1;
        sys.core[i].dcache.prefetch_buffer_writes = 1;
        sys.core[i].dcache.prefetch_buffer_reads = 1;
        sys.core[i].dcache.prefetch_buffer_hits = 1;
        sys.core[i].dcache.wbb_writes = 1;
        sys.core[i].dcache.wbb_reads = 1;
        //system.core?.BTB
        for (unsigned int j = 0; j < 20; j++) sys.core[i].BTB.BTB_config[j] = 1;
        sys.core[i].BTB.total_accesses = 1;
        sys.core[i].BTB.read_accesses = 1;
        sys.core[i].BTB.write_accesses = 1;
        sys.core[i].BTB.total_hits = 1;
        sys.core[i].BTB.total_misses = 1;
        sys.core[i].BTB.read_hits = 1;
        sys.core[i].BTB.write_hits = 1;
        sys.core[i].BTB.read_misses = 1;
        sys.core[i].BTB.write_misses = 1;
        sys.core[i].BTB.replacements = 1;
    }

    //system_L1directory
    for (unsigned int i = 0; i <= 63; i++) {

        for (unsigned int j = 0; j < 20; j++) sys.L1Directory[i].Dir_config[j] = 1;
        for (unsigned int j = 0; j < 20; j++) sys.L1Directory[i].buffer_sizes[j] = 1;
        sys.L1Directory[i].clockrate = 1;
//		sys.L1Directory[i].ports[20]=1;
        sys.L1Directory[i].device_type = 1;
        sys.L1Directory[i].vdd = 0;
        sys.L1Directory[i].power_gating_vcc = -1;
        strcpy(sys.L1Directory[i].threeD_stack, "default");
        sys.L1Directory[i].total_accesses = 1;
        sys.L1Directory[i].read_accesses = 1;
        sys.L1Directory[i].write_accesses = 1;
        sys.L1Directory[i].duty_cycle = 1;
    }
    //system_L2directory
    for (unsigned int i = 0; i <= 63; i++) {
        for (unsigned int j = 0; j < 20; j++) sys.L2Directory[i].Dir_config[j] = 1;
        for (unsigned int j = 0; j < 20; j++) sys.L2Directory[i].buffer_sizes[j] = 1;
        sys.L2Directory[i].clockrate = 1;
//		sys.L2Directory[i].ports[20]=1;
        sys.L2Directory[i].device_type = 1;
        sys.L2Directory[i].vdd = 0;
        sys.L2Directory[i].power_gating_vcc = -1;
        strcpy(sys.L2Directory[i].threeD_stack, "default");
        sys.L2Directory[i].total_accesses = 1;
        sys.L2Directory[i].read_accesses = 1;
        sys.L2Directory[i].write_accesses = 1;
        sys.L2Directory[i].duty_cycle = 1;
    }
    for (unsigned int i = 0; i <= 63; i++) {
        //system_L2
        for (unsigned int j = 0; j < 20; j++) sys.L2[i].L2_config[j] = 1;
        sys.L2[i].clockrate = 1;
        for (unsigned int j = 0; j < 20; j++) sys.L2[i].ports[j] = 1;
        sys.L2[i].device_type = 1;
        sys.L2[i].vdd = 0;
        sys.L2[i].power_gating_vcc = -1;
        strcpy(sys.L2[i].threeD_stack, "default");
        for (unsigned int j = 0; j < 20; j++) sys.L2[i].buffer_sizes[j] = 1;
        sys.L2[i].total_accesses = 1;
        sys.L2[i].read_accesses = 1;
        sys.L2[i].write_accesses = 1;
        sys.L2[i].total_hits = 1;
        sys.L2[i].total_misses = 1;
        sys.L2[i].read_hits = 1;
        sys.L2[i].write_hits = 1;
        sys.L2[i].read_misses = 1;
        sys.L2[i].write_misses = 1;
        sys.L2[i].replacements = 1;
        sys.L2[i].write_backs = 1;
        sys.L2[i].miss_buffer_accesses = 1;
        sys.L2[i].fill_buffer_accesses = 1;
        sys.L2[i].prefetch_buffer_accesses = 1;
        sys.L2[i].prefetch_buffer_writes = 1;
        sys.L2[i].prefetch_buffer_reads = 1;
        sys.L2[i].prefetch_buffer_hits = 1;
        sys.L2[i].wbb_writes = 1;
        sys.L2[i].wbb_reads = 1;
        sys.L2[i].duty_cycle = 1;
        sys.L2[i].merged_dir = false;
        sys.L2[i].homenode_read_accesses = 1;
        sys.L2[i].homenode_write_accesses = 1;
        sys.L2[i].homenode_read_hits = 1;
        sys.L2[i].homenode_write_hits = 1;
        sys.L2[i].homenode_read_misses = 1;
        sys.L2[i].homenode_write_misses = 1;
        sys.L2[i].dir_duty_cycle = 1;
    }
    for (unsigned int i = 0; i <= 63; i++) {
        //system_L3
        for (unsigned int j = 0; j < 20; j++) sys.L3[i].L3_config[j] = 1;
        sys.L3[i].clockrate = 1;
        for (unsigned int j = 0; j < 20; j++) sys.L3[i].ports[j] = 1;
        sys.L3[i].device_type = 1;
        sys.L3[i].vdd = 0;
        sys.L2[i].power_gating_vcc = -1;
        strcpy(sys.L3[i].threeD_stack, "default");
        for (unsigned int j = 0; j < 20; j++) sys.L3[i].buffer_sizes[j] = 1;
        sys.L3[i].total_accesses = 1;
        sys.L3[i].read_accesses = 1;
        sys.L3[i].write_accesses = 1;
        sys.L3[i].total_hits = 1;
        sys.L3[i].total_misses = 1;
        sys.L3[i].read_hits = 1;
        sys.L3[i].write_hits = 1;
        sys.L3[i].read_misses = 1;
        sys.L3[i].write_misses = 1;
        sys.L3[i].replacements = 1;
        sys.L3[i].write_backs = 1;
        sys.L3[i].miss_buffer_accesses = 1;
        sys.L3[i].fill_buffer_accesses = 1;
        sys.L3[i].prefetch_buffer_accesses = 1;
        sys.L3[i].prefetch_buffer_writes = 1;
        sys.L3[i].prefetch_buffer_reads = 1;
        sys.L3[i].prefetch_buffer_hits = 1;
        sys.L3[i].wbb_writes = 1;
        sys.L3[i].wbb_reads = 1;
        sys.L3[i].duty_cycle = 1;
        sys.L3[i].merged_dir = false;
        sys.L3[i].homenode_read_accesses = 1;
        sys.L3[i].homenode_write_accesses = 1;
        sys.L3[i].homenode_read_hits = 1;
        sys.L3[i].homenode_write_hits = 1;
        sys.L3[i].homenode_read_misses = 1;
        sys.L3[i].homenode_write_misses = 1;
        sys.L3[i].dir_duty_cycle = 1;
    }
    //system_NoC
    for (unsigned int i = 0; i <= 63; i++) {
        sys.NoC[i].clockrate = 1;
        sys.NoC[i].type = true;
        sys.NoC[i].chip_coverage = 1;
        sys.NoC[i].vdd = 0;
        sys.NoC[i].power_gating_vcc = -1;
        sys.NoC[i].has_global_link = true;
        strcpy(sys.NoC[i].topology, "default");
        sys.NoC[i].horizontal_nodes = 1;
        sys.NoC[i].vertical_nodes = 1;
        sys.NoC[i].input_ports = 1;
        sys.NoC[i].output_ports = 1;
        sys.NoC[i].virtual_channel_per_port = 1;
        sys.NoC[i].flit_bits = 1;
        sys.NoC[i].input_buffer_entries_per_vc = 1;
        sys.NoC[i].total_accesses = 1;
        sys.NoC[i].duty_cycle = 1;
        sys.NoC[i].route_over_perc = 0.5;
        for (unsigned int j = 0; j < 20; j++) sys.NoC[i].ports_of_input_buffer[j] = 1;
        sys.NoC[i].number_of_crossbars = 1;
        strcpy(sys.NoC[i].crossbar_type, "default");
        strcpy(sys.NoC[i].crosspoint_type, "default");
        //system.NoC?.xbar0;
        sys.NoC[i].xbar0.number_of_inputs_of_crossbars = 1;
        sys.NoC[i].xbar0.number_of_outputs_of_crossbars = 1;
        sys.NoC[i].xbar0.flit_bits = 1;
        sys.NoC[i].xbar0.input_buffer_entries_per_port = 1;
//		sys.NoC[i].xbar0.ports_of_input_buffer[20]=1;
        sys.NoC[i].xbar0.crossbar_accesses = 1;
    }
    //system_mem
    sys.mem.mem_tech_node = 1;
    sys.mem.device_clock = 1;
    sys.mem.capacity_per_channel = 1;
    sys.mem.number_ranks = 1;
    sys.mem.peak_transfer_rate = 1;
    sys.mem.num_banks_of_DRAM_chip = 1;
    sys.mem.Block_width_of_DRAM_chip = 1;
    sys.mem.output_width_of_DRAM_chip = 1;
    sys.mem.page_size_of_DRAM_chip = 1;
    sys.mem.burstlength_of_DRAM_chip = 1;
    sys.mem.internal_prefetch_of_DRAM_chip = 1;
    sys.mem.memory_accesses = 1;
    sys.mem.memory_reads = 1;
    sys.mem.memory_writes = 1;
    //system_mc
    sys.mc.mc_clock = 1;
    sys.mc.number_mcs = 1;
    sys.mc.peak_transfer_rate = 1;
    sys.mc.memory_channels_per_mc = 1;
    sys.mc.number_ranks = 1;
    sys.mc.req_window_size_per_channel = 1;
    sys.mc.IO_buffer_size_per_channel = 1;
    sys.mc.databus_width = 1;
    sys.mc.addressbus_width = 1;
    sys.mc.memory_accesses = 1;
    sys.mc.memory_reads = 1;
    sys.mc.memory_writes = 1;
    sys.mc.LVDS = true;
    sys.mc.type = 1;
    sys.mc.vdd = 0;
    sys.mc.power_gating_vcc = -1;
    //system_niu
    sys.niu.clockrate = 1;
    sys.niu.number_units = 1;
    sys.niu.type = 1;
    sys.niu.vdd = 0;
    sys.niu.power_gating_vcc = -1;
    sys.niu.duty_cycle = 1;
    sys.niu.total_load_perc = 1;
    //system_pcie
    sys.pcie.clockrate = 1;
    sys.pcie.number_units = 1;
    sys.pcie.num_channels = 1;
    sys.pcie.type = 1;
    sys.pcie.vdd = 0;
    sys.pcie.power_gating_vcc = -1;
    sys.pcie.withPHY = false;
    sys.pcie.duty_cycle = 1;
    sys.pcie.total_load_perc = 1;
    //system_flash_controller
    sys.flashc.mc_clock = 1;
    sys.flashc.number_mcs = 1;
    sys.flashc.vdd = 0;
    sys.flashc.power_gating_vcc = -1;
    sys.flashc.peak_transfer_rate = 1;
    sys.flashc.memory_channels_per_mc = 1;
    sys.flashc.number_ranks = 1;
    sys.flashc.req_window_size_per_channel = 1;
    sys.flashc.IO_buffer_size_per_channel = 1;
    sys.flashc.databus_width = 1;
    sys.flashc.addressbus_width = 1;
    sys.flashc.memory_accesses = 1;
    sys.flashc.memory_reads = 1;
    sys.flashc.memory_writes = 1;
    sys.flashc.LVDS = true;
    sys.flashc.withPHY = false;
    sys.flashc.type = 1;
    sys.flashc.duty_cycle = 1;
    sys.flashc.total_load_perc = 1;


    sys.repairCore.clock_rate = 1;
    sys.repairCore.opcode_width = 1;
    strcpy(sys.repairCore.FPU, "default");
    strcpy(sys.repairCore.ALU, "default");
    sys.repairCore.register_size = 32;
    sys.repairCore.num_registers = 10;
    sys.repairCore.total_instructions = 0;
    sys.repairCore.int_instructions = 0;
    sys.repairCore.fp_instructions = 0;
    sys.repairCore.load_instructions = 0;
    sys.repairCore.store_instructions = 0;
    sys.repairCore.total_cycles = 0;
    sys.repairCore.idle_cycles = 0;
    sys.repairCore.busy_cycles = 0;
    sys.repairCore.phy_int_regfile_reads = 0;
    sys.repairCore.phy_float_regfile_reads = 0;
    sys.repairCore.phy_float_regfile_writes = 0;
    sys.repairCore.phy_int_regfile_writes = 0;
    sys.repairCore.load_buffer_reads = 0;
    sys.repairCore.load_buffer_writes = 0;
    sys.repairCore.load_buffer_cams = 0;
    sys.repairCore.store_buffer_reads = 0;
    sys.repairCore.store_buffer_writes = 0;
    sys.repairCore.store_buffer_cams = 0;
    sys.repairCore.store_buffer_forwards = 0;
    sys.repairCore.main_memory_access = 0;
    sys.repairCore.main_memory_read = 0;
    sys.repairCore.main_memory_write = 0;
    sys.repairCore.pipeline_duty_cycle = 0;
    sys.repairCore.IFU_duty_cycle = 0;
    sys.repairCore.LSU_duty_cycle = 0;
    sys.repairCore.MemManU_I_duty_cycle = 0;
    sys.repairCore.MemManU_D_duty_cycle = 0;
    sys.repairCore.ALU_duty_cycle = 0;
    sys.repairCore.MUL_duty_cycle = 0;
    sys.repairCore.FPU_duty_cycle = 0;
    sys.repairCore.ALU_cdb_duty_cycle = 0;
    sys.repairCore.MUL_cdb_duty_cycle = 0;
    sys.repairCore.FPU_cdb_duty_cycle = 0;
    sys.repairCore.vdd = 0;
    sys.repairCore.power_gating_vcc = 0;

}
