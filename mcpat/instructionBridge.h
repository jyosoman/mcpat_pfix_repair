//
//

#ifndef CMCPAT_INSTRUCTIONBRIDGE_H
#define CMCPAT_INSTRUCTIONBRIDGE_H


#include <cacti/component.h>
#include "XML_Parse.h"
#include "array.h"

class InstructionBridge: public Component {
     public:
	ParseXML *XML;
    InputParameter interface_ip;
	ArrayST* buffer;
    InstructionBridge(ParseXML *XML_interface, InputParameter& interface_ip ):buffer(nullptr){buffer=new ArrayST(interface_ip);
    //XML_interface->sys.instructionBridge.buffer_size;
    };
    void compute(){};
    void displayEnergy(uint32_t indent = 0,int plevel = 100, bool is_tdp=true){};
    ~InstructionBridge(){};
};


#endif //CMCPAT_INSTRUCTIONBRIDGE_H
